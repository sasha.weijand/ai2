import numpy as np
import copy

from grid import Grid
from position import GridPosition


class OccupancyGrid(Grid):
	def __init__(self, rows, cols):
		"""
		:param rows: The number of rows that the OccupancyGrid should have.
		:param cols: The number of columns that the OccupancyGrid should have.
		"""

		Grid.__init__(self, rows, cols, 7)
		self.max_score = 15
		self.min_score = 0
		self.gro = GRO()
		self.last_robot_pos = None
		self.next_centroid = None
		self.terminate_worker = False
		self.terminate_main = False
		

	def update_region_I (self, pos):
		"""
		Updates a grid element that the laser has found to be occupied.
		This is done by applying a Growth Rate Operator mask of size 3x3 to a
		grid part of the same size, with the given coordinates in the middle.
		:param pos: A GridPosition
		"""
		
		self.set_value_of(pos, self.value_of(pos)+3)
		if self.value_of(pos) >= self.max_score:
			self.set_value_of(pos, self.max_score)
		self.set_value_of(pos, np.minimum(self.max_score, self.gro.apply(self.surrounding_of(pos, 0))))
		if self.value_of(pos) == 7:
			self.set_value_of(pos, 8)
			

	def update_region_II (self, pos):
		"""
		Updates a grid element that the laser has found to be empty.
		:param pos: A GridPosition.
		"""
		
		v = self.value_of(pos)
		if v != self.min_score:
			self.set_value_of(pos, v-1)
		if self.value_of(pos) == 7:
			self.set_value_of(pos, 6)
			

	def __str__(self):
		return str(self.grid)
		

	def get_deep_occupancy_grid_copy(self):
		"""
		Makes a deep copy of the entire occupancy_grid object.
		:return: A deep copy of the entire occupancy_grid object.
		"""

		return copy.deepcopy(self)
		

	def set_last_robot_pos(self, new_pos):
		"""
		Setter for the last position of the robot.
		:param new_pos: The latest robot position, a GridPosition.
		:return: N/A
		"""
		
		self.last_robot_pos = new_pos
		

	def set_next_centroid(self, centroid_pos):
		"""
		Setter for the last position of the robot.
		:param centroid_pos: The next centroid, a GridPosition.
		:return: N/A
		"""
		
		self.next_centroid = centroid_pos
		

class GRO(object):
	"""
	Growth Rate Operator. A 3x3 mask that can be applied to a 3x3 part of a HIMM-grid.
	"""
	
	def __init__(self):
		"""
		Fills the GRO with the number 0.5,
		except for the middle element, which is 1.
		"""

		self.mask = np.zeros(shape=(3, 3))
		self.mask.fill(0.5)
		self.mask[1][1] = 1.0

	def apply(self, grid_part):
		"""
		Adds up the grid element values multiplied by the mask's corresponding element value.
		:param grid_part: The grid part (3x3) to apply the GRO mask to.
		:return: N/A
		"""

		add = 0.0
		for row in range(0, 3):
			for col in range(0, 3):
				add += self.mask[col][row]*float(grid_part[col][row])
		return int(add)

	def __str__(self):
		return np.array2string(self.mask)


"""
Test code.
"""

def test_grid_update(verbose):
	rows = 5
	cols = 6
	grid = OccupancyGrid(rows, cols)

	grid.update_region_II(GridPosition(1,1))
	if verbose:
		print("updated (II) (row,col)=(1,1):")
		print(grid)

	grid.update_region_II(GridPosition(3, 1))
	if verbose:
		print("updated (II) (row,col)=(3,1):")
		print(grid)

	grid.update_region_II(GridPosition(1, 3))
	if verbose:
		print("updated (II) (row,col)=(1,3):")
		print(grid)

	grid.update_region_I(GridPosition(0,1))
	if verbose:
		print("updated (I)(row,col)=(0,1):")
		print(grid)
	grid.update_region_I(GridPosition(4,1))
	if verbose:
		print("updated (I) (row,col)=(4,1):")
		print(grid)
	grid.update_region_I(GridPosition(1,5))
	if verbose:
		print("updated (I) (row,col)=(1,5):")
		print(grid)
	grid.update_region_I(GridPosition(1,0))
	if verbose:
		print("updated (I) (row,col)=(1,0):")
		print(grid)
	grid.update_region_I(GridPosition(0,0))
	if verbose:
		print("updated (I) (row,col)=(0,0):")
		print(grid)
	grid.update_region_I(GridPosition(0,5))
	if verbose:
		print("updated (I) (row,col)=(0,5):")
		print(grid)
	grid.update_region_I(GridPosition(4,0))
	if verbose:
		print("updated (I) (row,col)=(4,0):")
		print(grid)
	grid.update_region_I(GridPosition(4,5))
	if verbose:
		print("updated (I) (row,col)=(4,5):")
		print(grid)

	print(grid)


if __name__ == '__main__':

	test_grid_update(True)


