import numpy as np
import copy

from grid import Grid
from position import GridPosition


class FrontierGrid(Grid):
	def __init__(self, rows, cols):
		"""
		:param rows: The number of rows that the OccupancyGrid should have.
		:param cols: The number of columns that the OccupancyGrid should have.
		"""

		Grid.__init__(self, rows, cols, 0)
		

	def mark_cell(self, row, col):		
		"""
		Marks a cell as a frontier cell.
		:param row: The row of the cell.
		:param col: The column of the cell.
		:return: N/A
		"""
		
		self.grid[row][col] = 1
		

	def unmark_cell(self, pos):
		"""
		Unmarks a cell as a frontier cell.
		:param row: The row of the cell.
		:param col: The column of the cell.
		:return: N/A
		"""
		
		self.set_value_of(pos, 0)
		

	def get_marked_neighbours_of(self, pos):
		"""
		Gets all marked neighbours of a cell.
		:param row: The row of the cell.
		:param col: The column of the cell.
		:return: A list of marked neighbours.
		"""
		
		all_neighbours = self.neighbours_of(pos)
		marked_neighbours = []

		for n in all_neighbours:
			if n[1] == 1:
				marked_neighbours.append(n[0])

		return marked_neighbours


