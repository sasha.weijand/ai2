# -*- coding: utf-8 -*-
import sys
import time
import threading as threading

from cartographer import Cartographer
from occupancy_grid import OccupancyGrid
from explorer import Explorer
from position import PointPosition
from show_map import ShowMap
import driver
from nav_utilities import NavUtilities
from path_calculator import PathCalculator

# Max and min positions for the whole factory environment.
ABSOLUTE_X_MIN = -64
ABSOLUTE_X_MAX = 54
ABSOLUTE_Y_MIN = -59
ABSOLUTE_Y_MAX = 59

CELL_SIZE = 1.0
MAX_GRID_VAL = 15
GRIDWORKER_UPDATE_INTERVAL = 0.1

# Example calls for Windows that give printouts in PowerShell:
# py ./mapper.py localhost:50000 -30 -20 40 50 1
# python3 ./mapper.py http://localhost:50000 -64 -59 54 59 1

# Example call for Linux/Windows. Can only print in the Linux terminal and doesn't show the map.

# ./mapper.sh http://yanovskaya.cs.umu.se:50000 -64 -59 54 59 0
# ./mapper.sh <Computer Address>:50000 -30 -20 40 50 0


def run():
	"""
	Main logic of the program. Starts the support threads and updates the map until exploration is complete.
	:return: N/A
	"""

	grid_lock = threading.Lock()

	# Declare objects of GridWorkerThread and PlannerReactThread class with lock.
	worker_thread = GridWorkerThread(grid_lock)
	planner_thread = PlanDriveReactThread(grid_lock)

	worker_thread.start()
	planner_thread.start()

	while 1:
		grid_lock.acquire()
		grid_robot_pos = o_grid.last_robot_pos
		next_centroid = o_grid.next_centroid
		grid_copy = o_grid.get_deep_grid_copy()
		grid_lock.release()

		show_map.update_map(grid_copy, MAX_GRID_VAL, grid_robot_pos, next_centroid)

		if o_grid.terminate_main:
			break

	worker_thread.join()
	planner_thread.join()

	print('Exploration complete.')


def check_arguments():
	"""
	Checks arguments for errors.
	:return: The "Show GUI" boolean, inputted minimum PointPosition and inputted max PointPosition.
	"""

	if len(sys.argv) != 7:
		print('Usage: python Mapper <url> <x_min> <y_min> <x_max> <y_max> <show_GUI>')
		exit(1)
	import http.client

	nav_url = sys.argv[1]
	if nav_url.startswith("http"):
		nav_url = nav_url[len("http://"):]
	mrds = http.client.HTTPConnection(nav_url)

	try:
		mrds.request('GET', '/lokarria/laser/echoes')
	except ConnectionRefusedError:
		print("Connection refused")
		quit(1)

	min_pos = PointPosition(sys.argv[2], sys.argv[3])
	if int(min_pos.x) < ABSOLUTE_X_MIN:
		print("X value of lower left corner outside of area.")
		exit(1)
	if int(min_pos.y) < ABSOLUTE_Y_MIN:
		print("Y value of lower left corner outside of area.")
		exit(1)

	max_pos = PointPosition(sys.argv[4], sys.argv[5])
	if int(max_pos.x) > ABSOLUTE_X_MAX:
		print("X value of upper right corner outside of area.")
		exit(1)
	if int(max_pos.y) > ABSOLUTE_Y_MAX:
		print("Y value of upper right corner outside of area.")
		exit(1)

	if int(min_pos.x) > int(max_pos.x):
		print("X value of lower left corner greater than X value of upper right corner.")
		exit(1)
	if int(min_pos.y) > int(max_pos.y):
		print("Y value of lower left corner greater than Y value of upper right corner.")
		exit(1)

	if int(sys.argv[6]) == 0:
		return 0, min_pos, max_pos, nav_url
	else:
		return 1, min_pos, max_pos, nav_url


def init_utility_objects():
	"""
	Initializes objects needed to run the program and returns them.
	:return: An instance of NavUtilities, OccupancyGrid, Cartographer and ShowMap.
	"""

	nav_local = NavUtilities(url, mini_pos.x, maxi_pos.y, CELL_SIZE)
	rows = int((int(maxi_pos.y) - int(mini_pos.y)) / CELL_SIZE)
	cols = int((int(maxi_pos.x) - int(mini_pos.x)) / CELL_SIZE)

	show_map_local = ShowMap(rows, cols, show_GUI)
	o_grid_local = OccupancyGrid(rows, cols)
	cart_local = Cartographer(o_grid_local, nav_local, mini_pos, maxi_pos, CELL_SIZE)

	return nav_local, o_grid_local, cart_local, show_map_local


class GridWorkerThread(threading.Thread):
	"""
	Thread class that updates the grid at a set interval until it gets a termination order from PlannerReactThread.
	"""

	def __init__(self, grid_lock):
		threading.Thread.__init__(self)
		self.grid_lock = grid_lock

	def run(self):
		"""
		Main function.
		:return: N/A
		"""

		while 1:
			self.grid_lock.acquire()
			robot_current_pos = nav.get_position()
			ret = cart.update(robot_current_pos)

			if ret == 1:
				o_grid.terminate_main = True
				self.grid_lock.release()
				exit(0)

			self.grid_lock.release()

			time.sleep(GRIDWORKER_UPDATE_INTERVAL)


class PlanDriveReactThread(threading.Thread):
	"""
	Thread class that governs the planning, driving and reactive parts of the program.
	Keeps going until there are no more centroids or until there are no safe paths to traverse.
	"""

	def __init__(self, grid_lock):
		threading.Thread.__init__(self)
		self.grid_lock = grid_lock
		self.path_calculator = PathCalculator()

	def run(self):
		"""
		Main function.
		:return: N/A
		"""

		time.sleep(1)
		robot_start_point_pos = nav.get_position()
		robot_start_grid_pos = nav.pos_to_grid(robot_start_point_pos)

		while 1:
			self.grid_lock.acquire()
			o_grid_copy = o_grid.get_deep_occupancy_grid_copy()
			self.grid_lock.release()

			last_robot_grid_pos = o_grid_copy.last_robot_pos
			last_robot_point = nav.grid_to_pos(last_robot_grid_pos)
			centroid_list = exp.explore(last_robot_point, o_grid_copy)

			if not centroid_list:
				# No more centroids to explore
				self.terminate_planner()

			failed_paths = 0
			for c in centroid_list:
				print("Chose centroid at: ", nav.grid_to_pos(c))

				start_time = time.time()
				path = self.path_calculator.calculate_path(last_robot_grid_pos, c, o_grid_copy)
				print("Time to calculate path to centroid: ", time.time() - start_time)
				if not path:
					failed_paths += 1
					if failed_paths == len(centroid_list):
						# None of the paths to the centroids were viable
						self.terminate_planner()
					else:
						print("Bad path, choosing next centroid.")
						continue

				self.grid_lock.acquire()
				o_grid.set_next_centroid(c)
				self.grid_lock.release()

				point_position_path = self.path_calculator.convert_path_to_points(path, nav)
				ret = driver.drive(nav, point_position_path)

				while ret == 1:
					# Driving towards start point after almost crashing
					self.grid_lock.acquire()
					o_grid_copy = o_grid.get_deep_occupancy_grid_copy()
					self.grid_lock.release()

					last_robot_grid_pos = o_grid_copy.last_robot_pos

					start_time = time.time()
					path = self.path_calculator.calculate_path(last_robot_grid_pos, robot_start_grid_pos, o_grid_copy)
					print("Time to calculate path to start: ", time.time() - start_time)

					self.grid_lock.acquire()
					o_grid.set_next_centroid(robot_start_grid_pos)
					self.grid_lock.release()

					while len(path) > 5:
						del path[-1]

					point_position_path = self.path_calculator.convert_path_to_points(path, nav)
					ret = driver.drive(nav, point_position_path)

				break

	def terminate_planner(self):
		"""
		Sets the termination flag for GridWorkerThread and terminates PlanDriveReactThread.
		:return:
		"""

		print("Planner finished.")
		self.grid_lock.acquire()
		o_grid.terminate_worker = True
		self.grid_lock.release()
		exit(0)


if __name__ == '__main__':
	show_GUI, mini_pos, maxi_pos, url = check_arguments()
	nav, o_grid, cart, show_map = init_utility_objects()
	exp = Explorer(nav)

	start_time = time.time()
	run()
	print("Total time for exploration: ", time.time() - start_time)


