import numpy as np

from occupancy_grid import OccupancyGrid
from position import GridPosition
from position import PointPosition
from grid import Grid


class PathCalculatorError(Exception): pass


class PathCalculator(object):

	def calculate_path(self, start_pos, goal_pos, o_grid_copy):
		"""
		Calculates a path between two GridPositions.
		:param start_pos: A start position to calculate the path from
		:param goal_pos: A goal position to calculate the path to
		:param o_grid_copy: A deep copy of the OccupancyGrid.
		:return: a list of GridPositions
		"""

		wavefront_grid = WaveFrontGrid(o_grid_copy, self.convert_pos(start_pos), self.convert_pos(goal_pos))
		print("Propagating wavefront grid.")
		wavefront_grid.propagate_wavefront()
		if not wavefront_grid.found_start:
			return []
		current_pos = wavefront_grid.start
		shortest_path = [self.revert_pos(current_pos)]
		print("Starting path calculation.")
		while current_pos != wavefront_grid.goal:
			nb_list = wavefront_grid.neighbours_of(current_pos)
			current_pos = self.find_best_neighbour(nb_list)
			shortest_path.append(self.revert_pos(current_pos))

			if len(shortest_path) > 1200:
				print("Long path!\n")
				return []

		if not shortest_path:
			raise PathCalculatorError("No path was calculated.")
		print("Path Calculated.")
		return shortest_path
		

	@staticmethod
	def convert_pos(position):
		return GridPosition(position.row + 1, position.col + 1)
		

	@staticmethod
	def revert_pos(position):
		return GridPosition(position.row - 1, position.col - 1)
		

	@staticmethod
	def convert_path_to_points(grid_path, nav):
		"""
		Finds a position in the list of neighbours that has the lowest associated value.
		:param neighbours: A list of neighbours (GridPosition, value)
		:return: A neighbour GridPosition.
		"""
	
		point_path = []
		for grid_pos in grid_path:
			point_path.append(nav.grid_to_pos(grid_pos))
		return point_path
	

	@staticmethod
	def find_best_neighbour(neighbours):
		"""
		Finds a position in the list of neighbours that has the lowest associated value.
		:param neighbours: A list of neighbours (GridPosition, value)
		:return: A neighbour GridPosition.
		"""

		filtered_nbs = [n for n in neighbours if n[1] > 1]
		if not filtered_nbs:
			filtered_nbs = [n for n in neighbours if n[1] == 0]
			return filtered_nbs[0][0]

		nbs_sorted_by_val = sorted(filtered_nbs, key=lambda tup: tup[1])
		lowest_value = nbs_sorted_by_val[0][1]
		lowest_neighbour = nbs_sorted_by_val[0][0]
		i = 0
		
		while i < len(nbs_sorted_by_val):
			if nbs_sorted_by_val[i][1] > lowest_value:
				break
			i += 1

		if i > 1:
			lowest_neighbour = nbs_sorted_by_val[int(i/2)][0]

		return lowest_neighbour


class WaveFrontGrid(Grid):

	def __init__(self, occupancy_grid, start_position, goal_position):
		"""
		A special grid to be used for path finding with wavefront propagation.
		It's an occupancy grid where the numbers >= 7 are converted to 1:s and the others are converted to 0:s.
		The grid is also surrounded by 1:s, to stop the wavefront from going outside the grid.
		:param occupancy_grid: The occupancy grid to search for a path in.
		:param start_position: The start GridPosition.
		:param goal_position: The goal GridPosition.
		"""
		
		self.found_start = False
		self.start = start_position
		self.goal = goal_position
		self.rows, self.cols = occupancy_grid.get_shape()
		Grid.__init__(self, self.rows + 2, self.cols + 2, 1)

		for i in range(0, self.rows):
			for j in range(0, self.cols):
				if occupancy_grid.value_of(GridPosition(i, j)) > 0:
					self.set_value_of(GridPosition(i + 1, j + 1), 1)
				else:
					self.set_value_of(GridPosition(i + 1, j + 1), 0)

	def propagate_wavefront(self):
		"""
		Fills the wavefront grid with numbers, increasing in levels from the goal position,
		until it reaches the start position.
		:return: N/A
		"""

		i = 0

		waves = [[self.goal]]
		while True:
			waves.append([])
			for cell in waves[i]:
				if self.value_of(cell) == 0:
					self.set_value_of(cell, i + 2)
				waves[i + 1].extend(self.unexplored_neighbours_of(cell, waves))
				if cell == self.start:
					self.set_value_of(cell, i + 2)
					return
				if self.start in self.neighbour_positions_of(cell):
					self.found_start = True
			if not waves[i+1]:
				break
			i += 1

	def unexplored_neighbours_of(self, cell, waves):
		"""
		Compares the neighbours of a cell to the wavefront list,
		returns a list of the neighbours that aren't already in the list,
		and that don't have the cell value 1.
		:param cell: The grid cell.
		:param waves: The wavefront list.
		:return: A list of neighbour GridPositions.
		"""

		nbs = [x[0] for x in self.neighbours_of(cell) if x[1] != 1]
		flat_list = [item for sublist in waves for item in sublist]
		res = [n for n in nbs if n not in flat_list]
		return res

	def __str__(self):
		return str(self.grid)


if __name__ == '__main__':
	# preparations
	cell_size = 1
	min_pos = PointPosition(-8, -8)
	max_pos = PointPosition(9, 9)
	row_amount = np.absolute(int((max_pos.y - min_pos.y) / cell_size))#17
	col_amount = np.absolute(int((max_pos.x - min_pos.x) / cell_size))#17
	o_grid = OccupancyGrid(row_amount, col_amount)

	o_grid.fill_area_with(GridPosition(0,0), GridPosition(17,17), one_value=0)
	o_grid.fill_area_with(GridPosition(5,5), GridPosition(8,8), one_value=15)
	o_grid.fill_area_with(GridPosition(3, 6), GridPosition(5, 8), one_value=15)
	# print(o_grid)

	start = GridPosition(3, 5)
	goal = GridPosition(10, 9)

	# actual testing
	p_calc = PathCalculator()
	print(o_grid)

	path = p_calc.calculate_path(start, goal, o_grid)
	for p in path:
		print(p)
