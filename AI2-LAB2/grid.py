import numpy as np
import copy

from position import GridPosition


class GridError(Exception):
	pass


class Grid(object):
	def __init__(self, rows, cols, standard_value):
		"""
		:param rows: The number of rows that the Grid should have.
		:param cols: The number of columns that the Grid should have.
		:param standard_value: The number to initialise the grid with.
		"""
		
		self.grid = np.ones(shape=(rows, cols), dtype=np.int8) * standard_value
		

	def get_sub_grid(self, min_pos, max_pos):
		"""
		Gets a sub grid of the grid.
		:param min_pos: GridPosition representing the upper left corner of the area to get.
		:param max_pos: GridPosition representing the lower right corner of the area to get.
		:return: The sub grid.
		"""

		return self.grid[min_pos.row:max_pos.row][min_pos.col:max_pos.col]
		

	def fill_area_with(self, min_pos, max_pos, sub_matrix=None, one_value=0):
		"""
		Fills a square area with the provided value.
		:param min_pos: GridPosition representing the upper left corner of the area to fill.
		:param max_pos: GridPosition representing the lower right corner of the area to fill.
		:param sub_matrix: An optional numpy matrix to fill the area with.
		:param one_value: Value to fill all grid cells in the area with.
		:return: N/A
		"""

		if sub_matrix is None:
			sub_matrix = np.ones(shape=(max_pos.row-min_pos.row, max_pos.col-min_pos.col), dtype=np.int8)*one_value
		self.grid[min_pos.row:max_pos.row, min_pos.col:max_pos.col] = sub_matrix
		

	def set_value_of(self, pos, val):
		"""
		Setter for the value of a grid cell.
		:param pos: The GridPosition to change.
		:param val: The new value.
		"""
		
		rows, cols = self.get_shape()
		if 0 <= pos.row < rows and 0 <= pos.col < cols:
			self.grid[pos.row][pos.col] = val
			

	def value_of(self, pos):
		"""
		Getter for the value of a grid cell.
		:param pos: A GridPosition.
		:return: The value at the desired position.
		"""
		
		rows, cols = self.get_shape()  
		if 0 <= pos.row < rows and 0 <= pos.col < cols:
			return self.grid[pos.row][pos.col]
		return None
		

	def get_shape(self):
		"""
		Getter for the shape of the grid.
		:return: The shape of the grid.
		"""
		
		return self.grid.shape
		

	def surrounding_of(self, pos, padding):
		"""
		Gets a 3x3 part of the grid with its values, surrounding the GridPosition pos, including pos itself.
		If it's a corner or edge piece, the missing part of the grid is filled with the padding number.
		:param pos: A GridPosition.
		:param padding: The number to fill the cells that are outside of the grid with.
		"""

		rows, cols = self.grid.shape
		enlarged_grid = np.ones(shape=(rows + 2, cols + 2), dtype=np.int8) * padding
		enlarged_grid[1:1 + rows, 1:1 + cols] = self.get_deep_grid_copy()

		if 0 <= pos.col < cols and 0 <= pos.row < rows:
			return enlarged_grid[[pos.row, pos.row + 1, pos.row + 2]][:, [pos.col, pos.col + 1, pos.col + 2]]

		raise GridError("Error getting grid part at position: ", pos.row, pos.col)

	def neighbour_values_of(self, pos):
		"""
		Get values of neighbours to grid cell.
		:param pos: A GridPosition.
		:return: A one-dimensional numpy array with the values of the neighbouring grid elements.
				If some of the neighbours are outside the grid, their values are 16.
		"""

		nbs_matrix = self.surrounding_of(pos, 16)
		nbs_array = nbs_matrix.reshape(9)

		return np.delete(nbs_array, 4)
		

	@staticmethod
	def neighbour_positions_of(pos):
		"""
		Get the positions of the neighbours for a given GridPosition.
		:param pos: A GridPosition.
		:return: A list of GridPositions representing the neighbours' positions.
			NOTE: Caller needs to check if the return values are within grid
		"""

		return [GridPosition(pos.row - 1, pos.col - 1),
				GridPosition(pos.row - 1, pos.col),
				GridPosition(pos.row - 1, pos.col + 1),
				GridPosition(pos.row, pos.col - 1),
				GridPosition(pos.row, pos.col + 1),
				GridPosition(pos.row + 1, pos.col - 1),
				GridPosition(pos.row + 1, pos.col),
				GridPosition(pos.row + 1, pos.col + 1)]
				

	def neighbours_of(self, pos):
		"""
		Gets the neighbours of a position, values and positions included.
		:param pos: A GridPosition.
		:return: A list of tuples (GridPosition, value) representing the neighbours.
		"""

		positions = self.neighbour_positions_of(pos)
		values = self.neighbour_values_of(pos)
		nbs = []
		for i in range(0, len(positions)):
			nbs.append((positions[i], values[i]))
		return nbs
		

	def __str__(self):
		np.set_printoptions(threshold=np.nan)
		np.set_printoptions(linewidth=np.nan)
		return np.array2string(self.grid)
		

	def get_deep_grid_copy(self):
		"""
		Makes a deep copy of the grid.
		:return: The deep copy of the grid.
		"""

		return copy.deepcopy(self.grid)
		

"""
Test code.
"""

def test_get_sourrounding():
	rows = 5
	cols = 6
	grid = Grid(rows, cols, 7)

	part = grid.surrounding_of(GridPosition(2, 2), 0)
	print("part (2,2)")
	print(part)

	# top left corner
	part = grid.surrounding_of(GridPosition(0, 0), 0)
	print("part (0,0), top left corner test:")
	print(part)

	# bottom left corner
	part = grid.surrounding_of(GridPosition(4, 0), 0)
	print("part (4,0), bottom left corner test:")
	print(part)

	# top right corner
	part = grid.surrounding_of(GridPosition(0, 5), 0)
	print("part (0,5), top right corner test:")
	print(part)

	# bottom right corner
	part = grid.surrounding_of(GridPosition(4, 5), 0)
	print("part (4,5), bottom right corner test:")
	print(part)

	# upper side test
	part = grid.surrounding_of(GridPosition(0, 1), 0)
	print("part (0,1), upper side test:")
	print(part)

	# left side test
	part = grid.surrounding_of(GridPosition(1, 0), 0)
	print("part (1,0), left side test:")
	print(part)

	# right side test
	part = grid.surrounding_of(GridPosition(1, 5), 0)
	print("part (1,5), right side test:")
	print(part)

	# bottom side test
	part = grid.surrounding_of(GridPosition(4, 1), 0)
	print("part (4,1), bottom side test:")
	print(part)


def test_get_neighbour_values ():
	grid = Grid(8, 9, 1)

	grid.set_value_of(GridPosition(0, 1), 5)
	grid.set_value_of(GridPosition(1, 0), 4)
	grid.set_value_of(GridPosition(1, 1), 6)
	grid.set_value_of(GridPosition(2, 1), 3)
	print(grid)
	nbs = grid.neighbour_values_of(GridPosition(0, 0))
	print("neighbour values of element at pos (row,col)=(0,0):")
	print(nbs)
	nbs = grid.neighbour_values_of(GridPosition(1, 1))
	print("neighbour values of element at pos (row,col)=(1,1):")
	print(nbs)


def test_get_neighbour_positions ():

	nbs = Grid.neighbour_positions_of(GridPosition(0, 0))
	print("neighbour positions of element at pos (row,col)=(0,0):")
	for n in nbs:
		print(n)

	nbs = Grid.neighbour_positions_of(GridPosition(1, 1))
	print("neighbour positions of element at pos (row,col)=(1,1):")
	for n in nbs:
		print(n)


def test_get_neighbours():
	grid = Grid(8, 9, 1)
	grid.set_value_of(GridPosition(0, 1), 5)
	grid.set_value_of(GridPosition(1, 0), 4)
	grid.set_value_of(GridPosition(1, 1), 6)
	grid.set_value_of(GridPosition(2, 1), 3)
	print(grid)

	nbs = grid.neighbours_of(GridPosition(0, 0))
	print("neighbours of element at pos (row,col)=(0,0):")
	for n in nbs:
		print(n[0], n[1])


def test_fill_area_with():
	grid = Grid(8, 9, 1)
	grid.fill_area_with(GridPosition(1,1), GridPosition(4,4), one_value=3)
	print(grid)

	grid.fill_area_with(GridPosition(4, 4), GridPosition(7, 7), sub_matrix=np.ones((3,3), np.int8)*3)
	print(grid)

if __name__ == '__main__':

	test_get_sourrounding()
	test_get_neighbour_values()
	test_get_neighbour_positions()
	test_get_neighbours()
	test_fill_area_with()