import http.client, json, time, sys, math
from math import sin, cos, pi, atan2
from position import PointPosition
from position import GridPosition
HEADERS = {"Content-type": "application/json", "Accept": "text/json"}


class UnexpectedResponse(Exception): pass


class NavUtilities(object):
	def __init__(self, url, x_min, y_max, cell_size):
		self.x_min = x_min
		self.y_max = y_max
		self.cell_size = cell_size
		self.url = url

	def get_laser(self):
		"""Requests the current laser scan from the MRDS server and parses it into a dict"""
		mrds = http.client.HTTPConnection(self.url)
		mrds.request('GET','/lokarria/laser/echoes')
		response = mrds.getresponse()
		if (response.status == 200):
			laser_data = response.read()
			response.close()
			return json.loads(laser_data.decode())
		else:
			return response

	def get_laser_angles(self):
		"""Requests the current laser properties from the MRDS server and parses it into a dict"""

		mrds = http.client.HTTPConnection(self.url)
		mrds.request('GET', '/lokarria/laser/properties')
		response = mrds.getresponse()
		if response.status == 200:
			laser_data = response.read()
			response.close()
			properties = json.loads(laser_data.decode())
			beam_count = int((properties['EndAngle'] - properties['StartAngle']) / properties['AngleIncrement'])
			a = properties['StartAngle']  # +properties['AngleIncrement']
			angles = []
			while a <= properties['EndAngle']:
				angles.append(a)
				a += pi / 180  # properties['AngleIncrement']
			# angles.append(properties['EndAngle']-properties['AngleIncrement']/2)
			return angles
		else:
			raise UnexpectedResponse(response)

	def post_speed(self, angular_speed, linear_speed):
		"""Sends a speed command to the MRDS server"""
		mrds = http.client.HTTPConnection(self.url)
		params = json.dumps({'TargetAngularSpeed':angular_speed, 'TargetLinearSpeed':linear_speed})
		mrds.request('POST','/lokarria/differentialdrive',params,HEADERS)
		response = mrds.getresponse()
		status = response.status
		#response.close()
		if status == 204:
			return response
		else:
			raise UnexpectedResponse(response)

	def get_pose(self):
		"""Reads the current position and orientation from the MRDS"""
		mrds = http.client.HTTPConnection(self.url)
		mrds.request('GET','/lokarria/localization')
		response = mrds.getresponse()
		if response.status == 200:
			pose_data = response.read()
			response.close()
			return json.loads(pose_data.decode('utf-8'))
		else:
			return UnexpectedResponse(response)

	def get_position(self):
		"""
		Gets the position of the robot as a PointPosition.
		:return: The position of the robot as a PointPosition.
		"""
		
		pose = self.get_pose()['Pose']['Position']
		return PointPosition(pose['X'], pose['Y'])

	def pos_to_grid(self, point_pos):
		"""
		Converts an (x,y) PointPosition to a (row,col) GridPosition.
		(x,y) is a point in a regular graph coordinate system.
		NOTE: Point coordinate (xmin,ymax) corresponds to the GridPosition (0,0).
		:param point_pos: A PointPosition.
		:return: A converted GridPosition.
		"""

		col = (point_pos.x - self.x_min) / self.cell_size
		row = (self.y_max - point_pos.y) / self.cell_size
		return GridPosition(row, col)

	def grid_to_pos(self, grid_pos):
		"""
		Converts a (row,col) GridPosition to an (x,y) PointPosition.
		:param grid_pos: A GridPosition.
		:return: A PointPosition.
		"""

		x = self.x_min + (grid_pos.col + 0.5) * self.cell_size
		y = self.y_max - (grid_pos.row + 0.5) * self.cell_size
		return PointPosition(x, y)

	def get_heading(self):
		"""Returns the XY Orientation as a heading unit vector"""
		return self.heading(self.get_pose()['Pose']['Orientation'])

	def heading(self, q):
		return self.rotate(q, {'X': 1.0, 'Y': 0.0, "Z": 0.0})

	def rotate(self, q, v):
		return self.vector(self.qmult(self.qmult(q, self.quaternion(v)), self.conjugate(q)))

	@staticmethod
	def quaternion(v):
		q = v.copy()
		q['W'] = 0.0
		return q

	@staticmethod
	def vector(q):
		v = {"X": q["X"], "Y": q["Y"], "Z": q["Z"]}
		return v

	@staticmethod
	def conjugate(q):
		qc = q.copy()
		qc["X"] = -q["X"]
		qc["Y"] = -q["Y"]
		qc["Z"] = -q["Z"]
		return qc

	@staticmethod
	def qmult(q1, q2):
		q = {"W": q1["W"] * q2["W"] - q1["X"] * q2["X"] - q1["Y"] * q2["Y"] - q1["Z"] * q2["Z"],
			"X": q1["W"] * q2["X"] + q1["X"] * q2["W"] + q1["Y"] * q2["Z"] - q1["Z"] * q2["Y"],
			"Y": q1["W"] * q2["Y"] - q1["X"] * q2["Z"] + q1["Y"] * q2["W"] + q1["Z"] * q2["X"],
			"Z": q1["W"] * q2["Z"] + q1["X"] * q2["Y"] - q1["Y"] * q2["X"] + q1["Z"] * q2["W"]}
		return q

	# Get the distance between two PointPositions p1 and p2.
	@staticmethod
	def get_distance(p1, p2):
		"""
		Calculates the distance between two PointPositions.
		:param p1: A PointPosition.
		:param p2: A PointPosition.
		:return: The distance between the two PointPositions.
		"""
		
		x = p1.x - p2.x
		y = p1.y - p2.y

		x = x*x
		y = y*y

		return math.sqrt(x+y)


	def face_object_radians(self, pos, target):
		"""
		Calculates the radians needed to rotate and face a PointPosition from another PointPosition.
		:param pos: Current location as a PointPosition
		:param target: Target PointPosition to rotate towards.
		:return: The radians to the target, assuming object at pos faces forwards, IE 0 radians.
		"""
		
		return math.atan2(target.y - pos.y, target.x - pos.x)

def test_pos_to_grid():

	nav_u = NavUtilities('localhost:50000', 0, 5, 1)

	p1 = PointPosition(0.5, 4.5)
	p2 = nav_u.pos_to_grid(p1)
	print("converted from point coordinate (", p1.x, ",", p1.y, ") to grid pos: (", p2.row, ",", p2.col, ")")

	p1 = PointPosition(0.0, 5.0)
	p2 = nav_u.pos_to_grid(p1)
	print("converted from point coordinate (", p1.x, ",", p1.y, ") to grid pos: (", p2.row, ",", p2.col, ")")

	p1 = PointPosition(0.5, 0.5)
	p2 = nav_u.pos_to_grid(p1)
	print("converted from point coordinate (", p1.x, ",", p1.y, ") to grid pos: (", p2.row, ",", p2.col, ")")

	p1 = PointPosition(0.0, 0.0)
	p2 = nav_u.pos_to_grid(p1)
	print("converted from point coordinate (", p1.x, ",", p1.y, ") to grid pos: (", p2.row, ",", p2.col, ")")

	p1 = PointPosition(5.0, -5.0)
	p2 = nav_u.pos_to_grid(p1)
	print("converted from point coordinate (", p1.x, ",", p1.y, ") to grid pos: (", p2.row, ",", p2.col, ")")

def test_grid_to_pos():

	nav_u = NavUtilities('localhost:50000', 0, 14, 2)
	p1 = GridPosition(5,1)
	p2 = nav_u.grid_to_pos(p1)
	print("converted from grid pos (", p1.row, ",", p1.col, ") to point coordinate: (", p2.x, ",", p2.y, ")")

	nav_u = NavUtilities('localhost:50000', 0, 14, 1)
	p1 = GridPosition(0, 0)
	p2 = nav_u.grid_to_pos(p1)
	print("converted from grid pos (", p1.row, ",", p1.col, ") to point coordinate: (", p2.x, ",", p2.y, ")")

if __name__ == '__main__':

	test_pos_to_grid()
	test_grid_to_pos()
