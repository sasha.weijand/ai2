import numpy as np
import math

import occupancy_grid as ogrid
from position import PointPosition
from position import GridPosition

from nav_utilities import NavUtilities

MAX_LASER_DISTANCE = 40


class Cartographer(object):
	def __init__(self, ogrid, navu, bottomleft, topright, cellsize):
		"""
		Handles the updating of the grid.
		:param ogrid: The grid object used for mapping (OccupancyGrid).
		:param navu: A navigator utilities object (NavUtilities).
		:param bottomleft: A PointPosition representing the bottom left corner of the grid.
		:param topright: A PointPosition representing the top right corner of the grid.
		:param cellsize: The size of the grid cells in meters.
		"""

		self.o_grid = ogrid
		self.bottom_left = bottomleft
		self.top_right = topright
		self.cell_size = cellsize
		self.nav = navu

	def update(self, curr_point):
		"""
		fires a salvo of laser beams and updates the OccupancyGrid
		according to the resulting data.
		:param curr_point: A PointPosition saying where the robot currently is.
		:return: 1 if the GridWorkerThread should terminate, 0 otherwise.
		"""

		if self.o_grid.terminate_worker:
			print("Worker done.")
			return 1

		laser_angles = self.nav.get_laser_angles()
		laser_salvo = self.nav.get_laser()['Echoes']
		heading = self.nav.get_heading()
		self.o_grid.set_last_robot_pos(self.nav.pos_to_grid(curr_point))

		for i in range(0, len(laser_angles)):

			laser_distance = laser_salvo[i]
			end_point = self.calc_end_point(curr_point, laser_distance, laser_angles[i], heading)
			grid_start_pos, grid_end_pos = self.get_grid_positions_for_laser(curr_point, laser_distance, end_point,
																			 laser_angles[i], heading)
			if not grid_start_pos and not grid_end_pos:
				continue

			grid_cells = self.get_grid_cells_in_line(grid_start_pos, grid_end_pos)

			if len(grid_cells) == 0:
				continue
			if grid_cells[0] == grid_end_pos:
				grid_cells.reverse()

			for cell in grid_cells:
				if cell == grid_cells[-1]:
					if laser_distance == MAX_LASER_DISTANCE or not self.within_area(end_point):
						self.o_grid.update_region_II(cell)
					else:
						self.o_grid.update_region_I(cell)
				else:
					self.o_grid.update_region_II(cell)
		return 0

	def calc_end_point(self, start_point, distance, angle, heading):
		"""
		Calculates the end PointPosition of the laser beam.
		:param start_point: A PointPosition saying where the robot currently is.
		:param distance: The laser beam distance.
		:param angle: The angle of the laser.
		:param heading: The heading of the robot.
		:return: A PointPosition.
		"""

		grid_laser_angle = self.get_grid_laser_angle(heading, angle)
		dx = distance * np.cos(grid_laser_angle)
		dy = distance * np.sin(grid_laser_angle)
		return PointPosition(start_point.x + dx, start_point.y+dy)

	def get_pos_at_border(self, start_point, angle, heading):
		"""
		Calculates the grid position closest to the edge of the map in a certain direction from the start point.
		:param start_point: A PointPosition saying where the robot currently is.
		:param angle: The angle of the laser.
		:param heading: The heading of the robot.
		:return: A GridPosition.
		"""

		pos = start_point
		grid_laser_angle = self.get_grid_laser_angle(heading, angle)
		dx = self.cell_size * np.cos(grid_laser_angle)
		dy = self.cell_size * np.sin(grid_laser_angle)
		while self.within_area(pos):
			pos = PointPosition(pos.x + dx, pos.y + dy)
		pos = PointPosition(pos.x-dx, pos.y - dy)

		return self.nav.pos_to_grid(pos)

	def get_grid_positions_for_laser (self, start_point, distance, end_point, angle, heading):
		"""
		Calculates which GridPosition the laser hits and returns it coupled with the GridPosition it starts at.
		:param heading: The heading of the robot.
		:param start_point: The current PointPosition of the robot and starting point of the laser.
		:param distance: The length of the laser. Maxes out at 40 meters.
		:param end_point: The end point of the laser beam, a PointPosition.
		:param angle: The angle of the laser beam relative to the robot's direction.
		:return: A tuple consisting of the GridPositions of the laser's
			start and end point, on the form: (start, end). Both are None if the laser has hit the same cell as the one
			the robot stands in.
		"""

		grid_start_pos = self.nav.pos_to_grid(start_point)
		if not self.within_area(end_point):
			if distance < self.cell_size:
				return grid_start_pos, grid_start_pos
			return grid_start_pos, self.get_pos_at_border(start_point, angle, heading)
		if self.nav.pos_to_grid(end_point) == grid_start_pos:
			return None, None
		return grid_start_pos, self.nav.pos_to_grid(end_point)

	def within_area(self, point):
		"""
		Checks that the PointPosition is within the grid.
		:param point: A PointPosition.
		:return: True if the PointPosition is within the limits of the grid, false otherwise.
		"""

		if self.bottom_left.x < point.x < self.top_right.x \
			and self.bottom_left.y < point.y < self.top_right.y:
			return True
		else:
			return False

	def get_grid_cells_in_line(self, grid_start_pos, grid_end_pos):
		"""
		Calculates an array of the grid cells affected by the line
		from grid_start_pos to grid_end_pos. Uses Bresenham's line algorithm.
		:param grid_end_pos: A GridPosition representing the end point of the line.
		:param grid_start_pos: A GridPosition representing the starting point of the line.
		:return: A list of GridPositions.
		NOTE: This list is sometimes backwards and MUST BE CORRECTED (reversed) if that's the case.
		"""

		# if dy < dx, i e if laser slope is low
		if (np.absolute(grid_end_pos.row - grid_start_pos.row)) < (np.absolute(grid_end_pos.col - grid_start_pos.col)):
			# if laser points left
			if grid_start_pos.col > grid_end_pos.col:
				grid_cells = self.__get_grid_cells_in_line_low(grid_end_pos, grid_start_pos)
			else:
				grid_cells = self.__get_grid_cells_in_line_low(grid_start_pos, grid_end_pos)
		else:
			# if laser points up
			if grid_start_pos.row > grid_end_pos.row:
				grid_cells = self.__get_grid_cells_in_line_high(grid_end_pos, grid_start_pos)
			else:
				grid_cells = self.__get_grid_cells_in_line_high(grid_start_pos, grid_end_pos)
		return grid_cells

	@staticmethod
	def get_grid_laser_angle(heading, laser_angle):
		"""
		Get the angle of the laser relative to the environment, not the robot.
		:param heading: The heading of the robot.
		:param laser_angle: The laser angle relative to the robot.
		:return: The laser angle relative to the environment.
		"""

		x_len = heading['X']
		y_len = heading['Y']
		robot_angle = math.atan2(y_len, x_len)
		return robot_angle + laser_angle

	@staticmethod
	def __get_grid_cells_in_line_low(grid_start_pos, grid_end_pos):
		"""
		Is used by get_grid_cells_in_line for slopes lower than 1.
		:param grid_end_pos: A grid position representing the end point.
		:param grid_start_pos: A grid position representing the starting point.
		:return: A list of grid positions in the format: (row, col)
		"""

		grid_cells = []
		d_row = grid_end_pos.row - grid_start_pos.row
		d_col = grid_end_pos.col - grid_start_pos.col
		yi = 1
		if d_row < 0:
			yi = -1
			d_row = -d_row
		D = 2*d_row - d_col
		row = grid_start_pos.row

		for col in range(grid_start_pos.col, grid_end_pos.col+1):
			grid_cells.append(GridPosition(row, col))
			if D > 0:
				row = row + yi
				D = D - 2*d_col
			D = D + 2*d_row
		return grid_cells

	@staticmethod
	def __get_grid_cells_in_line_high(grid_start_pos, grid_end_pos):
		"""
		Is used by get_grid_cells_in_line for slopes higher than 1.
		:param grid_end_pos: A grid position representing the end point.
		:param grid_start_pos: A grid position representing the starting point.
		:return: A list of grid positions in the format: (row, col)
		"""

		grid_cells = []
		d_row = grid_end_pos.row - grid_start_pos.row
		d_col = grid_end_pos.col - grid_start_pos.col
		xi = 1
		if d_col < 0:
			xi = -1
			d_col = -d_col
		D = 2*d_col - d_row
		col = grid_start_pos.col

		for row in range(grid_start_pos.row, grid_end_pos.row+1):
			grid_cells.append(GridPosition(row, col))
			if D > 0:
				col = col + xi
				D = D - 2*d_row
			D = D + 2*d_col
		return grid_cells

"""
Test code.
"""

def test_get_laser_positions():
	min_pos = PointPosition(-10, -11)
	max_pos = PointPosition(9, 12)
	cell_size = 1
	nav_u = NavUtilities('localhost:50000', min_pos.x, max_pos.y, cell_size)
	rows = np.absolute(int((max_pos.y - min_pos.y) / cell_size))
	cols = np.absolute(int((max_pos.x - min_pos.x) / cell_size))
	o_grid = ogrid.OccupancyGrid(rows, cols)
	cart = Cartographer(o_grid, nav_u, min_pos, max_pos, cell_size)
	h = nav_u.get_heading()
	pos = PointPosition(0, 2)

	print("from (row,col)=(0,2):")
	p0, p1 = cart.get_grid_positions_for_laser(pos, 4, math.pi / 2, h)
	print("laser of length 4 at angle 90 degrees:")
	print("from", p0, "to", p1)
	p0, p1 = cart.get_grid_positions_for_laser(pos, 4, math.pi / 4, h)
	print("laser of length 4 at angle 45 degrees:")
	print("from", p0, "to", p1)
	p0, p1 = cart.get_grid_positions_for_laser(pos, 4, 0, h)
	print("laser of length 4 at angle 0 degrees:")
	print("from", p0, "to", p1)
	p0, p1 = cart.get_grid_positions_for_laser(pos, 4, -math.pi / 4, h)
	print("laser of length 4 at angle -45 degrees:")
	print("from", p0, "to", p1)
	p0, p1 = cart.get_grid_positions_for_laser(pos, 4, -math.pi / 2, h)
	print("laser of length 4 at angle -90 degrees:")
	print("from", p0, "to", p1)
	p0, p1 = cart.get_grid_positions_for_laser(pos, 4, -3 * math.pi / 4, h)
	print("laser of length 4 at angle -135 degrees:")
	print("from", p0, "to", p1)
	p0, p1 = cart.get_grid_positions_for_laser(pos, 4, math.pi, h)
	print("laser of length 4 at angle 180 degrees:")
	print("from", p0, "to", p1)
	p0, p1 = cart.get_grid_positions_for_laser(pos, 4, 3 * math.pi / 4, h)
	print("laser of length 4 at angle 135 degrees:")
	print("from", p0, "to", p1)


def test_get_grid_cells_in_line():
	min_pos = PointPosition(-10, -11)
	max_pos = PointPosition(11, 12)
	cell_size = 1
	nav_u = NavUtilities('localhost:50000', min_pos.x, max_pos.y, cell_size)
	rows = np.absolute(int((max_pos.y - min_pos.y) / cell_size))
	cols = np.absolute(int((max_pos.x - min_pos.x) / cell_size))
	o_grid = ogrid.OccupancyGrid(rows, cols)
	cart = Cartographer(o_grid, nav_u, min_pos, max_pos, cell_size)

	start0 = GridPosition(0, 0)
	end = GridPosition(3, 2)
	elements = cart.get_grid_cells_in_line(start0, end)
	print_elements(start0, end, elements)
	
	end = GridPosition(2, 3)
	elements = cart.get_grid_cells_in_line(start0, end)
	print_elements(start0, end, elements)
	
	end = GridPosition(4, 2)
	elements = cart.get_grid_cells_in_line(start0, end)
	print_elements(start0, end, elements)
	
	end = GridPosition(2, 4)
	elements = cart.get_grid_cells_in_line(start0, end)
	print_elements(start0, end, elements)
	
	end = GridPosition(4, 0)
	elements = cart.get_grid_cells_in_line(start0, end)
	print_elements(start0, end, elements)
	
	end = GridPosition(0, 4)
	elements = cart.get_grid_cells_in_line(start0, end)
	print_elements(start0, end, elements)
	
	start1 = GridPosition(1, 1)
	
	end = GridPosition(4, 3)
	elements = cart.get_grid_cells_in_line(start1, end)
	print_elements(start1, end, elements)
	
	end = GridPosition(3, 4)
	elements = cart.get_grid_cells_in_line(start1, end)
	print_elements(start1, end, elements)
	
	end = GridPosition(5, 3)
	elements = cart.get_grid_cells_in_line(start1, end)
	print_elements(start1, end, elements)
	
	end = GridPosition(3, 5)
	elements = cart.get_grid_cells_in_line(start1, end)
	print_elements(start1, end, elements)
	
	end = GridPosition(5, 1)
	elements = cart.get_grid_cells_in_line(start1, end)
	print_elements(start1, end, elements)
	
	end = GridPosition(1, 5)
	elements = cart.get_grid_cells_in_line(start1, end)
	print_elements(start1, end, elements)

	start2 = GridPosition(5, 5)

	print("~60 degrees in upper left quadrant")
	end = GridPosition(3, 1)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("45 degrees in upper left quadrant")
	end = GridPosition(1, 1)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("~20 degrees in upper left quadrant")
	end = GridPosition(1, 3)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("~60 degrees in upper right quadrant")
	end = GridPosition(1, 7)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("45 degrees in upper right quadrant")
	end = GridPosition(1, 9)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("~20 degrees in upper right quadrant")
	end = GridPosition(3, 9)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("-~20 degrees in lower right quadrant")
	end = GridPosition(7, 9)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("-45 degrees in lower right quadrant")
	end = GridPosition(9, 9)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("-~60 degrees in lower right quadrant")
	end = GridPosition(9, 7)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("-~20 degrees in lower left quadrant")
	end = GridPosition(9, 3)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("-45 degrees in lower left quadrant")
	end = GridPosition(9, 1)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)

	print("-~60 degrees in lower left quadrant")
	end = GridPosition(7, 1)
	elements = cart.get_grid_cells_in_line(start2, end)
	print_elements(start2, end, elements)


def print_elements(p0, p1, grid_elements):
	print("grid elements from point", p0, "to point", p1, ":")
	for e in grid_elements:
		print(e)


def test_check_limits():
	min_pos = PointPosition(-8, -8)
	max_pos = PointPosition(9, 9)
	cell_size = 1
	nav_u = NavUtilities('localhost:50000', min_pos.x, max_pos.y, cell_size)
	row_amount = np.absolute(int((max_pos.y - min_pos.y) / cell_size))
	col_amount = np.absolute(int((max_pos.x - min_pos.x) / cell_size))
	o_grid = ogrid.OccupancyGrid(row_amount, col_amount)
	cart = Cartographer(o_grid, nav_u, min_pos, max_pos, cell_size)

	if cart.within_area(PointPosition(-9, -9)) or cart.within_area(PointPosition(10, 10)) \
		or cart.within_area(PointPosition(-9, 10)) or cart.within_area(PointPosition(10, -9)) \
		or cart.within_area(PointPosition(-9, 0)) or cart.within_area(PointPosition(10, 0)) \
		or cart.within_area(PointPosition(0, 10)) or cart.within_area(PointPosition(0, -9)):
		print("check_limits error")
	if cart.within_area(PointPosition(-8, 9)) and cart.within_area(PointPosition(9, 9)) \
		and cart.within_area(PointPosition(-8, -8)) and cart.within_area(PointPosition(9, -8)):
		print("check_limits success")


def test_update():

	min_pos = PointPosition(-8, -8)
	max_pos = PointPosition(9, 9)
	cell_size = 1
	nav_u = NavUtilities('localhost:50000', min_pos.x, max_pos.y, cell_size)
	row_amount = np.absolute(int((max_pos.y - min_pos.y) / cell_size))
	col_amount = np.absolute(int((max_pos.x - min_pos.x) / cell_size))
	o_grid = ogrid.OccupancyGrid(row_amount, col_amount)
	cart = Cartographer(o_grid, nav_u, min_pos, max_pos, cell_size)
	print("occupancy grid before update:")
	print(o_grid)
	robot_current_pos = nav_u.get_position()
	grid_pos = nav_u.pos_to_grid(robot_current_pos)

	print("running update from position (x,y)=", robot_current_pos, "= (row, col)=", grid_pos)
	cart.update(robot_current_pos)
	print("occupancy grid after update:")
	print(o_grid)


if __name__ == '__main__':

	test_check_limits()
	test_get_laser_positions()
	test_get_grid_cells_in_line()
	test_update()

