# -*- coding: utf-8 -*-
import time
import math
import numpy as np

from position import PointPosition

"""
	Environment corner positions using data from robot. 
	Top Left: X = -64 Y = 59
	Top Right: X = 54 Y = 59
	Lower Left: X = -64 Y = -59
	Lower Right: X = 54 Y = -59

"""


def drive(nav, path):
	"""
	Main function for driving along a path.
	:param nav: nav_utilities object used to interface with the robot.
	:param path: The path of PointPositions the robot is supposed to follow.
	:return: 1 or 0, depending on navigate_path.
	"""

	if len(path) < 2:
		return 0
	rotate_to_target(nav, path[1])
	return navigate_path(nav, path)


def rotate_to_target(navigator, pos):
	"""
	Rotates the robot towards the provided position. May take an extra second to turn for larger turns.
	:param pos: PointPosition to turn towards.
	:param navigator: nav_utilities object used to interface with the robot.
	:return: N/A
	"""

	res = navigator.get_pose()['Pose']['Position']
	robot_current_pos = PointPosition(res['X'], res['Y'])

	point_angle = navigator.face_object_radians(robot_current_pos, pos)
	h = navigator.get_heading()

	x_len = h['X']
	y_len = h['Y']

	robot_angle = math.atan2(y_len, x_len)
	final_angle = point_angle - robot_angle

	if math.fabs(final_angle) > math.pi:
		if final_angle > 0:
			final_angle = final_angle - (2 * math.pi)

		elif final_angle < 0:
			final_angle = (2 * math.pi) + final_angle

	navigator.post_speed(final_angle, 0)
	time.sleep(1)
	navigator.post_speed(0, 0)

	res = navigator.get_pose()['Pose']['Position']
	robot_current_pos = PointPosition(res['X'], res['Y'])

	point_angle = navigator.face_object_radians(robot_current_pos, pos)
	h = navigator.get_heading()

	x_len = h['X']
	y_len = h['Y']

	robot_angle = math.atan2(y_len, x_len)
	final_angle = point_angle - robot_angle

	if math.fabs(final_angle) > 0.1:
		navigator.post_speed(final_angle, 0)
		time.sleep(0.6)
		navigator.post_speed(0, 0)


def navigate_path(nav, path):
	"""
	Makes the robot navigate along a path. It stops if it's about to collide with an obstacle.
	:param nav:  nav_utilities object used to interface with the robot.
	:param path: The path of PointPositions the robot is supposed to follow.
	:return: 0 if the path has been followed and 1 if there's an obstacle.
	"""

	speed = 2
	i = 1

	while i < len(path):
		i += avoid_obstacle_near_side(nav, distance_limit=0.7)
		if i >= len(path):
			nav.post_speed(0, 0)
			print("Centroid reached.\n")
			return 0

		if found_obstacle_in_front(nav, distance_limit=0.7):
			print("Obstacle detected!")
			nav.post_speed(0, linear_speed=0)
			nav.post_speed(0, linear_speed=-1)
			time.sleep(1)
			nav.post_speed(0, linear_speed=0)
			rotate_to_target(nav, path[i-2])
			return 1

		robot_current_pos = nav.get_position()
		next_pos = path[i]
		point_angle = nav.face_object_radians(robot_current_pos, next_pos)

		h = nav.get_heading()
		x_len = h['X']
		y_len = h['Y']

		robot_angle = math.atan2(y_len, x_len)
		final_angle = point_angle - robot_angle

		if math.fabs(final_angle) > math.pi:
			if final_angle > 0:
				final_angle = final_angle - (2 * math.pi)

			elif final_angle < 0:
				final_angle = (2 * math.pi) + final_angle

		if math.fabs(final_angle) > 0.8:
			nav.post_speed(final_angle, speed / 3)
		else:
			nav.post_speed(final_angle, speed)

		time.sleep(0.05)
		if is_robot_near(nav, robot_current_pos, next_pos, 0.4):
			i += 1

	nav.post_speed(0, 0)
	print("Centroid reached.\n")
	return 0


def found_obstacle_in_front(nav, distance_limit):
	"""
	Checks if there's an obstacle in front of the robot.
	:param nav: nav_utilities object used to interface with the robot.
	:param distance_limit: The distance limit to check against.
	:return: True if there's an obstacle in range, false otherwise.
	"""

	laser_salvo = nav.get_laser()['Echoes']

	if laser_salvo[int(5*len(laser_salvo)/12)] < distance_limit:
		return True
	if laser_salvo[int(11*len(laser_salvo)/24)] < distance_limit:
		return True
	if laser_salvo[int(len(laser_salvo)/2)] < distance_limit:  # right in front
		return True
	if laser_salvo[int(13*len(laser_salvo)/24)] < distance_limit:
		return True
	if laser_salvo[int(7*len(laser_salvo)/12)] < distance_limit:
		return True

	return False


def avoid_obstacle_near_side(nav, distance_limit):
	"""
	Checks if there's an obstacle at the side of the robot and turns away if that's the case.
	:param nav: nav_utilities object used to interface with the robot.
	:param distance_limit: The distance limit to check against.
	:return: 0 if there's no obstacle, 2 otherwise.
	"""

	laser_salvo = nav.get_laser()['Echoes']

	if laser_salvo[5 * int(len(laser_salvo) / 6)] < distance_limit:
		nav.post_speed(-np.pi/4, 1)
		time.sleep(0.5)
		return 2
	if laser_salvo[4 * int(len(laser_salvo) / 6)] < distance_limit:
		nav.post_speed(-np.pi/2, 0.5)
		time.sleep(1)
		return 2
	if laser_salvo[2 * int(len(laser_salvo) / 6)] < distance_limit:
		nav.post_speed(np.pi/2, 0.5)
		time.sleep(1)
		return 2
	if laser_salvo[1 * int(len(laser_salvo) / 6)] < distance_limit:
		nav.post_speed(np.pi/4, 1)
		time.sleep(0.5)
		return 2
	return 0


def is_robot_near(nav, robot_pos, pos, distance_limit):
	"""
	Checks if the robot is near a certain PointPosition
	:param nav: nav_utilities object used to interface with the robot.
	:param robot_pos: A PointPosition saying where the robot currently is.
	:param pos: A PointPosition to check against.
	:param distance_limit: The limit to compare the actual distance to.
	:return: True if the distance is less than the limit, false otherwise.
	"""

	length = nav.get_distance(pos, robot_pos)

	return length < distance_limit

