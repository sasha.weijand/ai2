
class PointPosition(object):
	"""
	Exact position in the simulation environment, with the origin at the bottom left corner.
	"""
	
	def __init__(self, x, y):
		"""
		:param x: The x-value of the point position
		:param y: The y-value of the point position
		"""
		
		self.x = float(x)
		self.y = float(y)

	def __str__(self):
		return "(" + str(self.x) + ":" + str(self.y) + ")"

	def __eq__(self, other):
		return self.x == other.x and self.y == other.y


class GridPosition(object):
	"""
	Position in the grid, with the origin at the top left corner.
	"""
	def __init__(self, row, col):
		"""
		:param row: The row index of the grid position
		:param col: The column index of the grid position
		"""
		self.row = int(row)
		self.col = int(col)

	def __str__(self):
		return "(" + str(self.row) + ":" + str(self.col) + ")"

	def __eq__(self, other):
		return self.row == other.row and self.col == other.col
