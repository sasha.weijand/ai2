import threading as threading
import time
import numpy as np

import occupancy_grid
from nav_utilities import NavUtilities
from position import GridPosition, PointPosition
from frontier_grid import FrontierGrid


class FrontierCellFinder(threading.Thread):
	"""
	Thread class that iterates through a part of the grid to find frontier cells.
	"""	

	def __init__(self, start_row, end_row, o_grid_copy, frontier_grid):
		"""
		:param start_row: Row to start looking in.
		:param end_row: Row to stop looking in.
		:param o_grid_copy: A copy of the grid.
		:param frontier_grid: A FrontierGrid shared between FrontierCellFinder threads to mark cells in.
		"""
		
		threading.Thread.__init__(self)
		self.start_row = start_row
		self.end_row = end_row
		self.o_grid_copy = o_grid_copy
		self.frontier_grid = frontier_grid

	def run(self):
		"""
		Standard function.
		"""
		
		row, col = self.o_grid_copy.grid.shape
		i = self.start_row
		j = 0

		while i < self.end_row:
			while j < col:
				if self.o_grid_copy.grid[i][j] == 0:
					p = GridPosition(i, j)
					neighbour_value_list = self.o_grid_copy.neighbour_values_of(p)
					for n in neighbour_value_list:
						if n == 7:
							self.frontier_grid.mark_cell(i, j)
							break
				j += 1

			j = 0
			i += 1


class Explorer(object):
	def __init__(self, nav_util):
		self.nav_util = nav_util
		self.centroid_lists_sorted_by_size = 0

	def explore(self, current_pos, o_grid_copy):
		"""
		:param current_pos: The current PointPosition of the robot.
		:param o_grid_copy: A deep copy of the OccupancyGrid.
		:return: A centroid to drive to. None if there is nowhere to go.
		"""

		frontier_grid = self.find_frontier_cells(o_grid_copy)

		frontiers = self.create_frontiers(frontier_grid, o_grid_copy)
		self.filter_frontiers(frontiers)
		print("Amount of frontiers: ", len(frontiers))

		frontier_size_list = []
		centroids = []
		
		for f in frontiers:
			c = self.calculate_centroid(f)
			if o_grid_copy.value_of(c) < 7:
				centroids.append(c)
				frontier_size_list.append(len(f))

		if not centroids:
			return None

		start_time = time.time()
		sorted_centroids_list = self.sort_centroids_with_heuristic(current_pos, centroids, frontier_size_list)
		print("Time to sort centroids via heuristic: ", time.time() - start_time)
		
		return sorted_centroids_list
		

	@staticmethod
	def calculate_centroid(frontier):
		"""
		Calculates a centroid using a list of frontier cells.
		:param frontier: A list of GridPositions that make up a frontier.
		:return: The calculated centroid.
		"""
		
		row_c = col_c = count = 0
		
		for cell in frontier:
			row_c += cell.row
			col_c += cell.col
			count += 1
		
		if count == 0:
			print("Invalid frontier length: ", len(frontier))
			exit(1)
		row_c = row_c/count
		col_c = col_c/count

		return GridPosition(row_c, col_c)

	@staticmethod
	def find_frontier_cells(o_grid_copy):
		"""
		Launches threads that look through the grid and markes frontier cells in a shared FrontierGrid.
		Viable cells have a value of 0 and border a cell with a value of 7.
		:param o_grid_copy: A deep copy of the OccupancyGrid.
		:return: A FrontierGrid with marked frontier cells.
		"""

		rows, cols = o_grid_copy.grid.shape
		frontier_grid = FrontierGrid(rows, cols)

		first_finder = FrontierCellFinder(0, int(rows/2), o_grid_copy, frontier_grid)
		second_finder = FrontierCellFinder(int(rows/2), rows, o_grid_copy, frontier_grid)

		first_finder.start()
		second_finder.start()

		first_finder.join()
		second_finder.join()

		return frontier_grid


	@staticmethod
	def create_frontiers(frontier_grid, o_grid_copy):
		"""
		Groups frontier cells together to create frontiers. To avoid cells being used
		in multiple frontiers, they're unmarked when they get added to a frontier list.
		:param frontier_grid: FrontierGrid containing frontier cells.
		:param o_grid_copy:  A deep copy of the OccupancyGrid.
		:return: A list of frontiers.
		"""

		frontier_list = []
		intermediary_stack = []
		row, col = o_grid_copy.get_shape()

		i = 0
		j = 0

		while i < row:
			while j < col:

				if frontier_grid.grid[i][j] == 1:
					gp = GridPosition(i, j)
					intermediary_stack.append(gp)
					frontier_list.append([])

					while len(intermediary_stack) > 0:
						temp_cell = intermediary_stack.pop()
						frontier_grid.unmark_cell(temp_cell)
						frontier_list[-1].append(temp_cell)

						neighbour_position_list = frontier_grid.get_marked_neighbours_of(temp_cell)
						intermediary_stack.extend(neighbour_position_list)
				j += 1
			j = 0
			i +=1
		return frontier_list

	@staticmethod
	def filter_frontiers(frontier_list):
		"""
		Filters out frontiers smaller than 5 cells from the list.
		:param frontier_list: The list to filter.
		:return: N/A
		"""

		i = 0
		while i < len(frontier_list):
			if len(frontier_list[i]) < 2:
				del frontier_list[i]
			else:
				i += 1
				

	def sort_centroids_with_heuristic(self, current_pos, centroids, frontier_size_list):
		"""
		Sorts a list of centroids according to the cells-per-meters-to-traverse heuristic.
		:param current_pos: The PointPosition of the robot.
		:param centroids: The list of centroids to sort.
		:param frontier_size_list: A list of frontier sizes that corresponds to the centroid list.
		:return: A list of centroids sorted according to the heuristic.
		"""

		centroid_distance_list = []
		i = 0
		while i < len(centroids):
			distance = self.nav_util.get_distance(current_pos, self.nav_util.grid_to_pos(centroids[i]))
			if distance > 0:
				centroid_distance_list.append(distance)
				i += 1
			else:
				print("Centroid 0 meters away detected!")
				del(centroids[i])
				del(frontier_size_list[i])

		centroid_heuristic_value_list = []

		i = 0
		while i < len(centroids):
			centroid_heuristic_value_list.append(frontier_size_list[i] / (centroid_distance_list[i] / 2))
			i = i + 1

		sorted_centroids_list = []

		while len(centroids) > 0:
			i = 0
			best_value = 0
			best_index = 0
			while i < len(centroids):
				if centroid_heuristic_value_list[i] > best_value:
					best_value = centroid_heuristic_value_list[i]
					best_index = i
				i = i + 1
			sorted_centroids_list.append(centroids[best_index])
			del centroids[best_index]
			del centroid_heuristic_value_list[best_index]
		
		return sorted_centroids_list

"""
Test code.
"""

def test_calculate_centroid (exp):
	p1 = GridPosition(3, 4)
	p2 = GridPosition(2, 5)
	p3 = GridPosition(4, 3)
	front = [p1, p2, p3]
	print("calculating centroid for frontier with elements:")
	centroid = exp.calculate_centroid(front)
	print("result:", centroid)

	p1 = GridPosition(3, 0)
	p2 = GridPosition(2, 1)
	p3 = GridPosition(4, 0)
	p4 = GridPosition(1, 3)
	p5 = GridPosition(2, 2)
	p6 = GridPosition(0, 3)
	print("calculating centroid for frontier with elements:")
	front = [p1, p2, p3, p4, p5, p6]
	centroid = exp.calculate_centroid(front)
	print("result:", centroid)

	p1 = GridPosition(3, 0)
	p2 = GridPosition(2, 0)
	p3 = GridPosition(4, 0)
	p4 = GridPosition(1, 1)
	p5 = GridPosition(1, 2)
	p6 = GridPosition(1, 3)
	p7 = GridPosition(1, 4)
	p8 = GridPosition(4, 8)
	p9 = GridPosition(3, 7)
	p10 = GridPosition(3, 6)
	p11 = GridPosition(2, 5)
	front = [p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11]
	print("calculating centroid for frontier with elements:")
	centroid = exp.calculate_centroid(front)
	print("result:", centroid)

	p1 = GridPosition(0, 0)
	p2 = GridPosition(0, 1)
	p3 = GridPosition(1, 2)
	p4 = GridPosition(6, 2)
	p5 = GridPosition(1, 3)
	p6 = GridPosition(5, 3)
	p7 = GridPosition(2, 4)
	p8 = GridPosition(3, 4)
	p9 = GridPosition(4, 4)
	front = [p1, p2, p3, p4, p5, p6, p7, p8, p9]
	print("calculating centroid for frontier with elements:")
	centroid = exp.calculate_centroid(front)
	print("result:", centroid)

	p1 = GridPosition(0, 1)
	p2 = GridPosition(1, 1)
	p3 = GridPosition(1, 6)
	p4 = GridPosition(2, 1)
	p5 = GridPosition(2, 6)
	p6 = GridPosition(3, 2)
	p7 = GridPosition(3, 5)
	p8 = GridPosition(4, 3)
	p9 = GridPosition(4, 4)
	front = [p1, p2, p3, p4, p5, p6, p7, p8, p9]
	print("calculating centroid for frontier with elements:")
	centroid = exp.calculate_centroid(front)
	print("result:", centroid)

	p1 = GridPosition(2, 0)
	p2 = GridPosition(3, 0)
	p3 = GridPosition(1, 1)
	p4 = GridPosition(4, 1)
	p5 = GridPosition(5, 1)
	p6 = GridPosition(0, 2)
	p7 = GridPosition(6, 2)
	p8 = GridPosition(6, 3)
	p9 = GridPosition(6, 4)
	front = [p1, p2, p3, p4, p5, p6, p7, p8, p9]
	print("calculating centroid for frontier with elements:")
	centroid = exp.calculate_centroid(front)
	print("result:", centroid)


def test_create_frontiers(ogrid):
	p1 = GridPosition(2, 0)
	p2 = GridPosition(3, 0)
	p3 = GridPosition(1, 1)
	p4 = GridPosition(4, 1)
	p5 = GridPosition(5, 1)
	p6 = GridPosition(0, 2)
	p7 = GridPosition(6, 2)
	p8 = GridPosition(6, 3)
	p9 = GridPosition(6, 4)

	p10 = GridPosition(0, 4)
	p11 = GridPosition(1, 5)
	p12 = GridPosition(1, 6)
	p13 = GridPosition(2, 7)
	p14 = GridPosition(3, 8)


	frontier_cells = [p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14]
	frontiers = Explorer.create_frontiers(frontier_cells, ogrid)

	for f in frontiers:
		print("frontier:")
		for p in f:
			print(p)


if __name__ == '__main__':
	min_pos = PointPosition(-8, -8)
	max_pos = PointPosition(9, 9)
	cell_size = 1
	nav_u = NavUtilities('localhost:50000', min_pos.x, max_pos.y, cell_size)
	row_amount = np.absolute(int((max_pos.y - min_pos.y) / cell_size))
	col_amount = np.absolute(int((max_pos.x - min_pos.x) / cell_size))
	o_grid = occupancy_grid.OccupancyGrid(row_amount, col_amount)

	exp = Explorer(nav_u)
	test_calculate_centroid(exp)
	test_create_frontiers(o_grid)
