from OthelloAlgorithm import OthelloAlgorithm
from OthelloAction import OthelloAction
import math
from operator import itemgetter
import time
from MyEvaluator import MyEvaluator
from OthelloPosition import OthelloPosition


class OthelloAlgo(OthelloAlgorithm):

    def __init__(self):
        """
        Constructor.
        """
        self.evaluator = None
        self.search_depth = None
        self.max_time = None
        self.best_move = None
        self.time_limit = None
        self.done = False

    def set_evaluator(self, othello_evaluator):
        """
        Sets an OthelloEvaluator the algorithm is to use for heuristic evaluation.
        :param othello_evaluator: the OthelloEvaluator
        :return: Nothing
        """
        self.evaluator = othello_evaluator

    def set_search_depth(self, depth):
        """
        Sets the maximum search depth of the algorithm.
        :param depth: maximum search depth
        :return: Nothing
        """
        self.search_depth = depth

    def evaluate(self, othello_position):
        """
        Returns the OthelloAction the algorithm considered to be the best move given an OthelloPosition
        :param othello_position: The OthelloPosition to evaluate
        :return: The move to make as an OthelloAction
        """

        #othello_position.print_board()
        self.time_limit = time.time() + self.max_time
        self.search_depth = 0
        recent_move = None

        while time.time() < self.time_limit and not self.done:

            self.search_depth += 1
            if othello_position.white_to_move():  # if it's white's turn, start with max
                val = self.__max_value(othello_position, - math.inf, math.inf, 0)
            else:
                val = self.__min_value(othello_position, - math.inf, math.inf, 0)
            if val is not None:
                recent_move = self.best_move
                #print("                             new best move at depth", self.search_depth, " :", recent_move)
            else:
                break  # time is up

        if not recent_move:
            return OthelloAction(is_pass_move=True)
        return recent_move

    def set_max_time(self, max_time):
        """
        Sets the maximum time it can take to evaluate.
        :param max_time: an integer
        :return: N/A
        """
        self.max_time = max_time

    def __max_value(self, position, alpha, beta, current_depth):
        """
        Tries to find an action with maximum value outcome.
        :param position: the position to start evaluation from, an OthelloPosition
        :param alpha: if this is larger than beta, pruning occurs
        :param beta: if this is smaller than alpha, pruning occurs
        :param current_depth: the depth that the search is at now
        :return: the best value at current search depth, a numeric value
        """
        if time.time() > self.time_limit:
            return None
        if current_depth == self.search_depth:
            val = self.evaluator.evaluate(position)
            return val
        moves = position.get_moves()
        if len(moves) == 0:
            if current_depth == 0:
                self.done = True
            val = self.evaluator.evaluate(position)
            return val
        current_max_val = -math.inf
        current_best_move = None

        if current_depth == 0:
            if len(moves) == 1:
                self.done = True
                self.best_move = moves[0]
                return current_max_val
        for m in moves:
            val = self.__min_value(position.make_move(m), alpha, beta, current_depth + 1)
            if val is None:
                return None
            if val > current_max_val:
                current_best_move = m
            current_max_val = max(current_max_val, val)
            alpha = max(alpha, current_max_val)
            if alpha >= beta:
                break  # beta cutoff
        if current_depth == 0:
            self.best_move = current_best_move

        return current_max_val

    def __min_value(self, position, alpha, beta, current_depth):
        """
        Tries to find an action with minimum value outcome.
        :param position: the position to start evaluation from, an OthelloPosition
        :param alpha: if this is larger than beta, pruning occurs
        :param beta: if this is smaller than alpha, pruning occurs
        :param current_depth: the depth that the search is at now
        :return: the best value at current search depth, a numeric value
        """
        if time.time() > self.time_limit:
            return None
        if current_depth == self.search_depth:
            val = self.evaluator.evaluate(position)
            return val
        moves = position.get_moves()
        if len(moves) == 0:
            if current_depth == 0:
                self.done = True
            val = self.evaluator.evaluate(position)
            return val
        current_min_val = math.inf
        current_best_move = None

        if current_depth == 0:
            if len(moves) == 1:
                self.best_move = moves[0]
                self.done = True
                return current_min_val
        for m in moves:
            val = self.__max_value(position.make_move(m), alpha, beta, current_depth + 1)
            if val is None:
                return None
            if val < current_min_val:
                current_best_move = m
            current_min_val = min(current_min_val, val)
            beta = min(beta, current_min_val)
            if alpha >= beta:
                break  # alpha cutoff
        if current_depth == 0:
            self.best_move = current_best_move

        return current_min_val


def test_othello_algo_negative(position_string, expected_result):
    algo = OthelloAlgo()
    algo.set_evaluator(MyEvaluator())
    algo.set_max_time(2)
    assert str(algo.evaluate(OthelloPosition(position_string))) != expected_result, 'should not result in ' + expected_result


def test_othello_algo_positive(position_string, expected_result):
    algo = OthelloAlgo()
    algo.set_evaluator(MyEvaluator())
    algo.set_max_time(2)
    assert str(algo.evaluate(OthelloPosition(position_string))) == expected_result, 'should result in ' + expected_result


if __name__ == '__main__':
    test_othello_algo_positive('WXXXXXXXXXXXXXXXEXXXXXXXXXEXXXEXEXXXXXXXEXEXEXEEEEXXXEEEEEEXEEEEE', 'pass')

    test_othello_algo_positive('BOOOOOOOOOOXXXOOOOOOOXOOOOOOOOOOOOXOOOOOOOXXOXOOOOOOXEOOOOOOOOOEE', '(8,7)')

    test_othello_algo_negative('BEEEEEEEEEEEEEEEEEEEEOEEEEEEOOEEEEEEXOEEEEEEEEEEEEEEEEEEEEEEEEEEE', 'pass')

    test_othello_algo_negative('BEEOEEEEEEEEOEEEEEEOOOEEEEEOXXEEEEOOOOOOEEEOOOEEEEEEXEEEEEEEXEEEE', 'pass')
    test_othello_algo_negative('BEEOEEEEEEEEOEEEEEEOOOEEEEEOXXEEEEOOOOOOEEEOOOEEEEEEXEEEEEEEXEEEE', '(7,2)')
    test_othello_algo_negative('BEEOEEEEEEEEOEEEEEEOOOEEEEEOXXEEEEOOOOOOEEEOOOEEEEEEXEEEEEEEXEEEE', '(2,2)')

    test_othello_algo_positive('BOOOOOOOOOOXXXOOOOOOOXOOOOOXOOOOOOXOOOOOOOOXXXOOOOOEEEOOOEEEEEEEE', '(8,1)')

    # test_othello_algo_positive('BOOOOOOOOXOOOOOOOOOOOOOOOOOOXOOOOOOOXOOOXOOOOOXXOEOOOEEEEEEEEEEEE', '(8,1)')

    test_othello_algo_positive("BEEEEEEEEEOEOEEEEEEOOOOOEEEEXXEEEEEXOOEEEEXEEEEEEEEEEEEEEEEEEEEEE", '(1,1)')

    test_othello_algo_negative('BEEEEEEEEEEEEEEEEEEOOOEEEEEEOOEEEEEEOXXEEEEOEEEEEEEEEEEEEEEEEEEEE', '(2,2)')

    test_othello_algo_negative('BEEEEEEEEEXEEEEEEEEXOOEEEEEEXOEEEEEEOOOOEEEOEEEEEEEEEEEEEEEEEEEEE', '(2,4)')

    # test_othello_algo_negative('BOXOEEEEEEOEOEEEEOOOOOEEEEEEOOEEEEEEOOOOEEEOEEEEEEEEEEEEEEEEEEEEE', '(1,4)')

    test_othello_algo_negative('WOXXXOOXOOXXXOXOOOXOXXOXOOOXXOXXXOXOXXXXXOOXXXXXXOXXXXXEXOOOOXEEE', '(7,7)')
    # test_othello_algo_positive('WOXXXOOXOOXXXOXOOOXOXXOXOOOXXOXXXOXOXXXXXOOXXXXXXOXXXXXEXOOOOXEEE', '(8,6)')

    # test_othello_algo_positive('BOOOOOOEEEOOOOOOEEOOOOOOOOOOOOEXEEOOOXXOXEEOOOOOOEOXXXXEXOOOOOOOE', '(1,8)')

    test_othello_algo_negative('BEOXOEEEEEEOOEEOEEOOOOOEOEEOOOEOEEXXOXOOEEEOOOOEEEEOOEEEEEEEOEEEE', '(2,2)')

    # test_othello_algo_positive('BEOEOEEEEEEOOEEOEEEOOOOEOEEOOOEOEEXXXXOOEEEOOOOEEEEOOEEEEEEEOEEEE', '(1,1)')

    print("all tests succeeded!")