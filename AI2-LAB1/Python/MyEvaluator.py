from OthelloEvaluator import OthelloEvaluator
#import math
from OthelloPosition import OthelloPosition


class MyEvaluator(OthelloEvaluator):

    def __init__(self):
        self.__init_static_weights()
        self.__init_directions()
        self.pos = None
        self.corners_left = 4

    def evaluate(self, othello_position):
        """

        :param othello_position: an OthelloPosition
        :return: A number describing the value of some othello position (i e board configuration)
        """
        self.pos = othello_position
        corners_captured = self.__eval_corners_captured()

        if self.corners_left != 0:
            return 45 * corners_captured + \
                   40 * self.__eval_near_empty_corner() + \
                   5 * self.__eval_potential_mobility() + \
                   5 * self.__eval_static_weights() + \
                   5 * self.__eval_mobility()

        return 50 * self.__eval_potential_mobility() + \
               35 * self.__eval_mobility() + \
               15 * self.__eval_coin_parity()

    def __eval_coin_parity(self):
        """

        :return: the difference between the amount of white and black coins on the board.
        """
        b = 0
        w = 0
        for row in self.pos.board:
            for item in row:
                if item == 'W':
                    w += 1
                if item == 'B':
                    b += 1
        if w + b == 0:
            return 0
        return 100 * (w - b) / (w + b)

    def __eval_mobility(self):
        """

        :return:
        """
        w, b = self.get_amount_of_moves()
        if w + b == 0:
            return 0
        return 100 * (w - b) / (w + b)

    def get_amount_of_moves(self):
        """

        :return: two numbers
            The number of moves white and black has respectively
        """
        my_moves = self.pos.get_moves()
        opponent_moves = self.pos.get_moves(opponent=True)

        if self.pos.white_to_move():
            return len(my_moves), len(opponent_moves)

        return len(opponent_moves), len(my_moves)

    def __eval_potential_mobility(self):
        w, b = self.find_adjacent_empty()
        if w + b == 0:
            return 0
        return 100 * (b - w) / (w + b)

    def find_adjacent_empty(self):
        """

        :return: two numbers
            The number of empty squares adjacent to white and black player respectively.
            Some of the empty squares may be counted more than once.
        """
        w = 0
        b = 0
        for row in range(1, self.pos.BOARD_SIZE + 1):
            for col in range(1, self.pos.BOARD_SIZE+1):
                if self.pos.board[row][col] == 'W':
                    w += self.pos.amount_of_empty_neighbours(row, col)
                if self.pos.board[row][col] == 'B':
                    b += self.pos.amount_of_empty_neighbours(row, col)
        return w, b

    def __eval_corners_captured(self):
        """

        :return: The relative amount of captured corners for white compared to black.
        """
        w, b = self.__get_white_and_black_corners()

        self.corners_left = 4 - (w+b)
        if (w+b) == 0:
            return 0
        return 100 * (w - b) / (w+b)

    def __eval_near_empty_corner(self):
        """
        Returns values based on the number of coins each player has next to an empty corner.
        :return: the relative amount of black coins compared to white that are next to an empty corner.

        """
        self.init_corners()

        w = 0
        b = 0

        for corner in self.corners:
            if self.pos.board[corner[0][0]][corner[0][1]] == 'E':
                for direction in corner[1]:
                    if self.pos.board[corner[0][0] + direction[0]][corner[0][1] + direction[1]] == 'W':
                        w += 1
                    elif self.pos.board[corner[0][0] + direction[0]][corner[0][1] + direction[1]] == 'B':
                        b += 1
        # values are sent to transformation in reverse because many b is an advantage for w and vice versa
        if (w+b) == 0:
            return 0
        return 100 * (b - w) / (b + w)

    def __get_white_and_black_corners(self):
        """

        :return: the amount of corners captured by white and black respectively.
        """
        l = [
        self.pos.board[self.pos.BOARD_SIZE][self.pos.BOARD_SIZE],
        self.pos.board[1][self.pos.BOARD_SIZE],
        self.pos.board[1][1],
        self.pos.board[self.pos.BOARD_SIZE][1]
        ]
        return l.count("W"), l.count("B")

    def __eval_corner_stable(self):
        """
        Returns values based on the number of corner-stable coins each player has.
        :return: The relative amount of corner stable coins for white compared to black.
        """
        self.init_corners()

        w = 0
        b = 0

        for corner in self.corners:
            # check neighbours in different directions, count how far you get before opponent is found
            color = self.pos.board[corner[0][0]][corner[0][1]]
            if color == 'W':
                w += self.get_count_of_color_in_all_directions(color, corner)
            elif color == 'B':
                b += self.get_count_of_color_in_all_directions(color, corner)

        if w + b == 0:
            return 0
        return 100 * (w - b) / (w+b)

    def get_count_of_color_in_all_directions(self, color, corner):
        """
        Counts the distance to the opponent for a given player (b or w)
            in a given corner.
        :param color:
        :param corner: a tuple on the form ((r,c), (x, y, z)), where (r,c) are the
            row and column of the corner, and (x, y, z) are the possible directions
            to go in from that corner.
        :param opponent: boolean
            set to True if it's the opponents 'corner situation' that is
            evaluated (the default is False).
        :return: the number of coins that are stable in a corner
        """
        d = 0
        #print("getting count for color", color, "and corner", corner[0])
        cor = corner[0]
        for direction in corner[1]:
            #print("getting count for dir ", direction)
            d += self.pos.count_of_same_color_in_direction (color=color, row=cor[0], col=cor[1], direction=direction)

        return d - 2  # compensating because 3 directions

    def __init_directions(self):
        """
        Initializes a dict of possible directions.
        """
        self.directions = {'w':(0,1),'sw':(1,1),'s':(1,0),'se':(1,-1),
        'e':(0,-1),'ne':(-1,-1),'n':(-1,0),'nw':(-1,1)}

    def init_corners(self):
        """

        :return: a list of corner tuples
            which is a tuple on the form ((r,c), (x, y, z)), where (r,c) are the
            row and column of the corner, and (x, y, z) are the possible directions
            to go in from that corner.
        """
        self.corners = [((self.pos.BOARD_SIZE, self.pos.BOARD_SIZE),
            (self.directions['n'], self.directions['e'], self.directions['ne'])),
            ((1, self.pos.BOARD_SIZE), (self.directions['s'], self.directions['e'], self.directions['se'])),
            ((1, 1), (self.directions['w'], self.directions['s'], self.directions['sw'])),
            ((self.pos.BOARD_SIZE, 1), (self.directions['n'], self.directions['w'], self.directions['nw']))]

    def __eval_static_weights(self):
        b = 0
        w = 0

        row_nr = -1
        for row in self.pos.board:
            col = -1
            for item in row:
                if item == 'W':
                    w += self.table[row_nr][col]
                    # print("added val ", self.table[row_nr][col], "from (",row_nr,",", col,") in table to w")
                if item == 'B':
                    b += self.table[row_nr][col]
                    # print("added val ", self.table[row_nr][col], "from (",row_nr,",", col,") in table to b")
                col += 1
            row_nr += 1
        # print('result: w=', w, 'b=',b)

        if w + b == 0:
            return 0
        return 100 * (w - b) / (w + b)

    def __init_static_weights(self):
        first = [20, -3, 11, 8, 8, 11, -3, 20]
        second = [-3, -7, -4, 1, 1, -4, -7, -3]
        third = [11, -4, 2, 2, 2, 2, -4, 11]
        fourth = [8, 1, 2, -3, -3, 2, 1, 8]
        #first = [20,0,5,5,5,5,0,20]
        #second = [0,0,0,0,0,0,0,0]
        #third = [5,0,0,0,0,0,0,5]
        #fourth = [5,0,0,0,0,0,0,5]
        self.table = [first,second,third,fourth,fourth,third,second,first]


def test_count_of_color_in_dir ():
    ev = MyEvaluator()
    op = OthelloPosition('WXXXXXXXXXXXXXXXEXXXXXXXXXEXXXEXEXXXXXXXEXEXEXEEEEXXXEEEEEEXEEEEE')
    op.print_board()
    ev.pos = op
    ev.init_corners()
    print("corner values for corner 2:", ev.corners[2])
    b = op.count_of_same_color_in_direction('B', ev.corners[2][0][0], ev.corners[2][0][1], ev.corners[2][1][0])
    print("amount of blacks from upper left corner to the right:", b)
    b = op.count_of_same_color_in_direction('B', ev.corners[2][0][0], ev.corners[2][0][1], ev.corners[2][1][1])
    print("amount of blacks from upper left corner downwards:", b)
    b = op.count_of_same_color_in_direction('B', ev.corners[2][0][0], ev.corners[2][0][1], ev.corners[2][1][2])
    print("amount of blacks from upper left diagonally:", b)
    b = op.count_of_same_color_in_direction('B', ev.corners[1][0][0], ev.corners[1][0][1], ev.corners[1][1][0])
    print("amount of blacks from upper right corner downwards:", b)
    b = op.count_of_same_color_in_direction('B', ev.corners[1][0][0], ev.corners[1][0][1], ev.corners[1][1][1])
    print("amount of blacks from upper right corner to the left:", b)
    b = op.count_of_same_color_in_direction('B', ev.corners[1][0][0], ev.corners[1][0][1], ev.corners[1][1][2])
    print("amount of blacks from upper right corner diagonally:", b)

    b = ev.get_count_of_color_in_all_directions(color='B', corner=ev.corners[2])
    print("count of black in all directions from upper left corner:", b)


def test_eval_mobility ():
    ev = MyEvaluator()
    op = OthelloPosition('WOOOOOOOOOOXXXOOOOOOOXOOOOOXOOOOOOXOOOOOOOOXXXOOOOXEEEOOOXEEEEEEE')
    op.print_board()
    ev.pos = op
    w, b = ev.get_amount_of_moves()
    print("amount of moves for w:", w)
    print("amount of moves for b:", b)


def test_eval_potential_mobility ():
    ev = MyEvaluator()
    op = OthelloPosition('WOOOOOOOOOOXXXOOOOOOOXOOOOOXOOOOOOXOOOOOOOOXXXOOOOXEEEOOOXEEEEEEE')
    op.print_board()
    ev.pos = op
    w, b = ev.find_adjacent_empty()
    print("amount of empty spots adjacent to w:", w)
    print("amount of empty spots adjacent to b:", b)


if __name__ == '__main__':
    # test_count_of_color_in_dir()
    test_eval_mobility()
    test_eval_potential_mobility()