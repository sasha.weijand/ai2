class OthelloAction(object):
    """
      This class represents a 'move' in a game.
      The move is simply represented by two integers: the row and the column where the player puts the marker and a
      boolean to mark if it is a pass move or not.
      In addition, the OthelloAction has a field where the estimated value of the move can be stored during
      computations.
    """

    def __init__(self, row=None, col=None, is_pass_move=False):
        """
        Creates a new OthelloAction for (row, col) with value 0.
        :param row: Row
        :param col: Column
        :param is_pass_move: True if it is a pass move
        """
        self.is_pass_move = is_pass_move
        self.value = None

        self.row = row
        self.col = col

    def get_action(self):
        return self.row, self.col

    def set_value(self, val):
        self.value = val

    def get_value(self):
        return self.value

    def get_neighbour_in_direction(self, direction):
        """
        Takes a direction in the form of a tuple (x,y),
        example:    (1,1) means southwest
                    (0,-1) means east
        """
        return OthelloAction(self.row + direction[0], self.col + direction[1])

    def check_bounds(self, bound):
        if 0 < self.row < bound and 0 < self.col < bound:
            return True
        return False

    def is_pass(self):
        return self.is_pass_move

    def is_empty(self):
        return not self.is_pass_move and self.row == None and self.col == None

    def __str__(self):
        """
        Gives move on the format (3,6) or Pass in a string
        :return: string
        """
        if self.is_pass_move:
            return "pass"
        else:
            return "(" + str(self.row) + "," + str(self.col) + ")"
