import numpy as np
from OthelloAction import OthelloAction


class OthelloPosition(object):
	"""
	This class is used to represent game positions. It uses a 2-dimensional char array for the board
	and a Boolean to keep track of which player has the move.

	Author: Ola Ringdahl
	"""

	def __init__(self, board_str=""):
		"""
		Creates a new position according to str. If str is not given all squares are set to E (empty)
		:param board_str: A string of length 65 representing the board. The first character is W or B, indicating which
		player is to move. The remaining characters should be E (for empty), O (for white markers), or X (for black
		markers).
		"""
		self.BOARD_SIZE = 8
		self.whiteToMove = True
		self.board = np.array([['E' for col in range(self.BOARD_SIZE + 2)] for row in range(self.BOARD_SIZE + 2)])
		if len(list(board_str)) >= 65:
			if board_str[0] == 'W':
				self.whiteToMove = True
			else:
				self.whiteToMove = False
			for i in range(1, len(list(board_str))):
				col = ((i - 1) % 8) + 1
				row = (i - 1) // 8 + 1
				# For convenience we use W and B in the board instead of X and O:
				if board_str[i] == 'X':
					self.board[row][col] = 'B'
				elif board_str[i] == 'O':
					self.board[row][col] = 'W'
			if self.__board_is_uninitialized():
				self.initialize()
				pass

	def initialize(self):
		"""
		Initializes the position by placing four markers in the middle of the board.
		:return: Nothing
		"""
		self.board[self.BOARD_SIZE // 2][self.BOARD_SIZE // 2] = 'W'
		self.board[self.BOARD_SIZE // 2 + 1][self.BOARD_SIZE // 2 + 1] = 'W'
		self.board[self.BOARD_SIZE // 2][self.BOARD_SIZE // 2 + 1] = 'B'
		self.board[self.BOARD_SIZE // 2 + 1][self.BOARD_SIZE // 2] = 'B'
		self.whiteToMove = True

	def make_move(self, action):
		"""
		Perform the move suggested by the OthelloAction action and return the new position. Observe that this also
		changes the player to move next.
		:param action: The move to make as an OthelloAction
		:return: The OthelloPosition resulting from making the move action in the current position.
		"""

		new_position = self.clone()
		if not action.is_pass():
			row, col = action.get_action()
			if self.__check_north(row, col):
				i = row - 1
				while self.is_opponent_square(i, col):
					new_position.__set_tile(i, col)
					i -= 1
			if self.__check_south(row, col):
				i = row + 1
				while self.is_opponent_square(i, col):
					new_position.__set_tile(i, col)
					i += 1
			if self.__check_east(row, col):
				i = col + 1
				while self.is_opponent_square(row, i):
					new_position.__set_tile(row, i)
					i += 1
			if self.__check_west(row, col):
				i = col - 1
				while self.is_opponent_square(row, i):
					new_position.__set_tile(row, i)
					i -= 1
			if self.__check_north_east(row, col):
				i = row - 1
				j = col + 1
				while self.is_opponent_square(i, j):
					new_position.__set_tile(i, j)
					i -= 1
					j += 1
			if self.__check_north_west(row, col):
				i = row - 1
				j = col - 1
				while self.is_opponent_square(i, j):
					new_position.__set_tile(i, j)
					i -= 1
					j -= 1
			if self.__check_south_east(row, col):
				i = row + 1
				j = col + 1
				while self.is_opponent_square(i, j):
					new_position.__set_tile(i, j)
					i += 1
					j += 1
			if self.__check_south_west(row, col):
				i = row + 1
				j = col - 1
				while self.is_opponent_square(i, j):
					new_position.__set_tile(i, j)
					i += 1
					j -= 1
			new_position.__set_tile(row, col)

		new_position.whiteToMove = not self.whiteToMove
		return new_position

	def __get_own_tile (self):
		if self.whiteToMove:
			return 'W'
		return 'B'

	def __set_tile(self, x, y):
		self.board[x][y] = self.__get_own_tile()

	def get_moves(self, opponent=False):
		"""
		Get all possible moves for the current position
		:return: A list of OthelloAction representing all possible moves in the position. If the
		list is empty, there are no legal moves for the player who has the move.
		"""
		moves = []
		if opponent:
			self.whiteToMove = not self.whiteToMove
		for i in range(self.BOARD_SIZE):
			for j in range(self.BOARD_SIZE):
				if self.__is_candidate(i + 1, j + 1) and self.__is_move_possible(i + 1, j + 1):
					moves.append(OthelloAction(i+1, j+1))
		if opponent:
			self.whiteToMove = not self.whiteToMove
		return moves

	# def get_diff(self, next_position):
	#     for i in range(0,self.BOARD_SIZE):
	#         for j in range(0,self.BOARD_SIZE):
	#             if self.board[i+1][j+1] == 'E' and next_position.board[i+1][j+1] != 'E':
	#                 return OthelloAction(i+1,j+1)
	#     return OthelloAction(None, None)

	def __is_empty(self, row, col):
		if self.board[row][col] == 'E':
			return True
		return False

	def is_end_of_game(self):
		if len(self.get_moves()) == 0:
			return True
		# arr = np.array(self.board)
		# if 'E' not in arr:
		#     # print("full")
		#     return True
		# if 'B' not in arr or 'W' not in arr:
		#     # print("one color")
		#     return True
		return False

	def __board_is_uninitialized(self):
		if self.board[self.BOARD_SIZE // 2][self.BOARD_SIZE // 2] == 'E' or \
		self.board[self.BOARD_SIZE // 2 + 1][self.BOARD_SIZE // 2 + 1] == 'E' or \
		self.board[self.BOARD_SIZE // 2][self.BOARD_SIZE // 2 + 1] == 'E' or \
		self.board[self.BOARD_SIZE // 2 + 1][self.BOARD_SIZE // 2] == 'E':
			return True
		return False

	def __is_candidate(self, row, col):
		"""
		Check if a position is a candidate for a move (not empty and has a neighbour)
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is a candidate
		"""
		if not self.__is_empty(row, col):
			return False
		if self.has_neighbour(row, col):
			return True
		return False

	def __is_move_possible(self, row, col):
		"""
		Check if it is possible to do a move from this position
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is possible to do a move
		"""
		if row < 1 or row > self.BOARD_SIZE or col < 1 or col > self.BOARD_SIZE:
			return False
		if self.__check_north(row, col):
			return True
		if self.__check_north_east(row, col):
			return True
		if self.__check_east(row, col):
			return True
		if self.__check_south_east(row, col):
			return True
		if self.__check_south(row, col):
			return True
		if self.__check_south_west(row, col):
			return True
		if self.__check_west(row, col):
			return True
		if self.__check_north_west(row, col):
			return True

	def __check_north(self, row, col):
		"""
		Check if it is possible to do a move to the north from this position
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is possible to do a move
		"""
		if not self.is_opponent_square(row - 1, col):
			return False
		i = row - 2
		while i > 0:
			if self.board[i][col] == 'E':
				return False
			if self.is_own_square(i, col):
				return True
			i -= 1
		return False

	def __check_north_east(self, row, col):
		"""
		Check if it is possible to do a move to the north east from this position
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is possible to do a move
		"""
		if not self.is_opponent_square(row - 1, col + 1):
			return False
		i = 2
		while row - i > 0 and col + i <= self.BOARD_SIZE:
			if self.board[row - i][col + i] == 'E':
				return False
			if self.is_own_square(row - i, col + i):
				return True
			i += 1
		return False

	def __check_north_west(self, row, col):
		"""
		Check if it is possible to do a move to the north west from this position
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is possible to do a move
		"""
		if not self.is_opponent_square(row - 1, col - 1):
			return False
		i = 2
		while row - i > 0 and col - i > 0:
			if self.board[row - i][col - i] == 'E':
				return False
			if self.is_own_square(row - i, col - i):
				return True
			i += 1
		return False

	def __check_south(self, row, col):
		"""
		Check if it is possible to do a move to the south from this position
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is possible to do a move
		"""
		if not self.is_opponent_square(row + 1, col):
			return False
		i = row + 2
		while i <= self.BOARD_SIZE:
			if self.board[i][col] == 'E':
				return False
			if self.is_own_square(i, col):
				return True
			i += 1
		return False

	def __check_south_east(self, row, col):
		"""
		Check if it is possible to do a move to the south east from this position
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is possible to do a move
		"""
		if not self.is_opponent_square(row + 1, col + 1):
			return False
		i = 2
		while row + i <= self.BOARD_SIZE and col + i <= self.BOARD_SIZE:
			if self.board[row + i][col + i] == 'E':
				return False
			if self.is_own_square(row + i, col + i):
				return True
			i += 1
		return False

	def __check_south_west(self, row, col):
		"""
		Check if it is possible to do a move to the south west from this position
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is possible to do a move
		"""
		if not self.is_opponent_square(row + 1, col - 1):
			return False
		i = 2
		while row + i <= self.BOARD_SIZE and col - i > 0:
			if self.board[row + i][col - i] == 'E':
				return False
			if self.is_own_square(row + i, col - i):
				return True
			i += 1
		return False

	def __check_west(self, row, col):
		"""
		Check if it is possible to do a move to the west from this position
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is possible to do a move
		"""
		if not self.is_opponent_square(row, col - 1):
			return False
		i = col - 2
		while i > 0:
			if self.board[row][i] == 'E':
				return False
			if self.is_own_square(row, i):
				return True
			i -= 1
		return False

	def __check_east(self, row, col):
		"""
		Check if it is possible to do a move to the east from this position
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it is possible to do a move
		"""
		if not self.is_opponent_square(row, col + 1):
			return False
		i = col + 2
		while i <= self.BOARD_SIZE:
			if self.board[row][i] == 'E':
				return False
			if self.is_own_square(row, i):
				return True
			i += 1
		return False

	def is_opponent_square(self, row, col):
		"""
		Check if the position is occupied by the opponent
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if opponent square
		"""
		if self.whiteToMove and self.board[row][col] == 'B':
			return True
		if not self.whiteToMove and self.board[row][col] == 'W':
			return True
		return False

	def is_own_square(self, row, col):
		"""
		Check if the position is occupied by the tile
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if it's your own square
		"""
		if not self.whiteToMove and self.board[row][col] == 'B':
			return True
		if self.whiteToMove and self.board[row][col] == 'W':
			return True
		return False

	def get_neighbours (self, row, col):
		"""
		Gets a list of the contents in the adjacent squares.
		:param row:
		:param col:
		:return: a list of characters (can be 'E', 'B', or 'W')
		"""

		if 1 < row < self.BOARD_SIZE and 1 < col < self.BOARD_SIZE:
			return [self.board[row - 1][col],
				self.board[row - 1, col + 1],
				self.board[row - 1][col - 1],
				self.board[row][col - 1],
				self.board[row][col + 1],
				self.board[row + 1][col - 1],
				self.board[row + 1][col + 1],
				self.board[row + 1][col]]
		if row == 1 and col == 1:
			return [self.board[row][col + 1],
				self.board[row + 1][col + 1],
				self.board[row + 1][col]]
		if row == self.BOARD_SIZE and col == 1:
			return [self.board[row - 1][col],
				self.board[row - 1, col + 1],
				self.board[row][col + 1]]
		if row == 1 and col == self.BOARD_SIZE:
			return [self.board[row][col - 1],
				self.board[row + 1][col - 1],
				self.board[row + 1][col]]
		if row == self.BOARD_SIZE and col == self.BOARD_SIZE:
			return [self.board[row - 1][col],
				self.board[row - 1][col - 1],
				self.board[row][col - 1]]
		if row == 1:
			return [self.board[row][col - 1],
				self.board[row][col + 1],
				self.board[row + 1][col - 1],
				self.board[row + 1][col + 1],
				self.board[row + 1][col]]
		if col == 1:
			return [self.board[row - 1][col],
				self.board[row - 1, col + 1],
				self.board[row][col + 1],
				self.board[row + 1][col + 1],
				self.board[row + 1][col]]
		if row == self.BOARD_SIZE:
			return [self.board[row - 1][col],
				self.board[row - 1, col + 1],
				self.board[row - 1][col - 1],
				self.board[row][col - 1],
				self.board[row][col + 1]]
		if col == self.BOARD_SIZE:
			return [self.board[row - 1][col],
				self.board[row - 1][col - 1],
				self.board[row][col - 1],
				self.board[row + 1][col - 1],
				self.board[row + 1][col]]
		return []

	def has_neighbour(self, row, col):
		"""
		Check if the position has any non-empty adjacent squares
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if has neighbours
		"""
		return any(s != 'E' for s in self.get_neighbours(row, col))

	def amount_of_empty_neighbours(self, row, col):
		"""
		Check if the position has any non-empty adjacent squares
		:param row: The row of the board position
		:param col: The column of the board position
		:return: True if has neighbours
		"""
		return sum(1 for i in self.get_neighbours(row, col) if i == 'E')

	def count_of_same_color_in_direction (self, color, row, col, direction):
		if self.board[row][col] == color:
			return 1 + self.count_of_same_color_in_direction (color, row + direction[0], col + direction[1], direction)
		return 0

	def white_to_move(self):
		"""
		Check which player's turn it is
		:return: True if the first player (white) has the move, otherwise False
		"""
		return self.whiteToMove

	def clone(self):
		"""
		Copy the current position
		:return: A new OthelloPosition, identical to the current one.
		"""
		ot = OthelloPosition("")
		ot.board = np.copy(self.board)
		ot.whiteToMove = self.whiteToMove
		# print("OT Size: ", self.board.shape)
		return ot

	def print_board(self):
		"""
		Prints the current board. Do not use when running othellostart (it will crash)
		:return: Nothing
		"""
		print_board = np.array([[' ' for col in range(self.BOARD_SIZE)] for row in range(self.BOARD_SIZE)])
		for row in range(0, self.BOARD_SIZE):
			for col in range(0, self.BOARD_SIZE):
				if self.board[row+1][col+1] == 'B':
					print_board[row][col] = 'X'
				elif self.board[row+1][col+1] == 'W':
					print_board[row][col] = 'O'

		if self.white_to_move():
			print("it's white's turn")
		else:
			print("it's black's turn")
		# print(''.join([''.join(print_board[i]) for i in range(0, self.BOARD_SIZE)]))
		print(print_board)


def __str__(self):
		strs = [''.join(self.board[i]) for i in range(1, self.BOARD_SIZE+1)]
		return ''.join(strs)


def test_neighbours():
	op = OthelloPosition('WXXXXXXXXXXXXXXXEXXXXXXXXXEXXXEXEXXXXXXXEXEXEXEEEEXXXEEEEEEXEEEEE')
	op.print_board()
	print("neighbours for (8,8):", op.get_neighbours(8, 8))
	print("empty neighbours for (8,8):", op.amount_of_empty_neighbours(8, 8))
	print("(8,8) has neighbours:", op.has_neighbour(8, 8))
	print("neighbours for (1,1):", op.get_neighbours(1, 1))
	print("empty neighbours for (1,1):", op.amount_of_empty_neighbours(1, 1))
	print("(1,1) has neighbours:", op.has_neighbour(1, 1))
	op = OthelloPosition('BOOOOOOOOOOXXXOOOOOOOXOOOOOOOOOOOOXOOOOOOOXXOXOOOOOOXEOOOOOOOOOEE')
	op.print_board()

	print("(7,2) has neighbours:", op.has_neighbour(7, 2))
	print("neighbours for (7,2):", op.get_neighbours(7, 2))
	print("empty neighbours for (7,2):", op.amount_of_empty_neighbours(7, 2))

	print("(1,1) has neighbours:", op.has_neighbour(1, 1))
	print("neighbours for (1,1):", op.get_neighbours(1, 1))
	print("empty neighbours for (1,1):", op.amount_of_empty_neighbours(1, 1))

	print("neighbours for (7,7):", op.get_neighbours(7, 7))
	print("empty neighbours for (7,7):", op.amount_of_empty_neighbours(7, 7))
	print("(7,7) has neighbours:", op.has_neighbour(7, 7))

def test_make_move():
	print("test 1:")
	op = OthelloPosition('BOOOOOOOOOOXXXOOOOOOOXOOOOOOOOOOOOXOOOOOOOXXOXOOOOOOXEOOOOOOOOOEE')
	op.print_board()
	print("black making move (8,7):")
	new_op = op.make_move(OthelloAction(8,7))
	new_op.print_board()

	print("\ntest 2:")
	op = OthelloPosition('BEEEEEEEEEEEEEEEEEEEEOEEEEEEOOEEEEEEXOEEEEEEEEEEEEEEEEEEEEEEEEEEE')
	op.print_board()
	print("black making move (3,6):")
	new_op = op.make_move(OthelloAction(3, 6))
	new_op.print_board()

	print("\ntest 3:")
	op = OthelloPosition('BEEOEEEEEEEEOEEEEEEOOOEEEEEOXXEEEEOOOOOOEEEOOOEEEEEEXEEEEEEEXEEEE')
	op.print_board()
	print("black making move (4,1):")
	new_op = op.make_move(OthelloAction(4, 1))
	new_op.print_board()

	print("\ntest 4:")
	op = OthelloPosition('BEEOEEEEEEEEOEEEEEEOOOEEEEEOXXEEEEOOOOOOEEEOOOEEEEEEXEEEEEEEXEEEE')
	op.print_board()
	print("black making move (1,4):")
	new_op = op.make_move(OthelloAction(1, 4))
	new_op.print_board()

	print("\ntest 5:")
	op = OthelloPosition('BEEOEEEEEEEEOEEEEEEOOOEEEEEOXXEEEEOOOOOOEEEOOOEEEEEEXEEEEEEEXEEEE')
	op.print_board()
	print("black making move (6,6):")
	new_op = op.make_move(OthelloAction(6, 6))
	new_op.print_board()

if __name__ == '__main__':
	# test_neighbours()
	test_make_move()