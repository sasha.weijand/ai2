import sys
from OthelloPosition import OthelloPosition
from OthelloAlgo import OthelloAlgo
from MyEvaluator import MyEvaluator


if len(sys.argv) < 3:
    print("too few arguments")
    exit()

if len(sys.argv[1]) != 65:
    print("position string is of wrong size")
    exit()

algorithm = OthelloAlgo()
algorithm.set_evaluator(MyEvaluator())
algorithm.set_max_time(int(sys.argv[2]))
print(algorithm.evaluate(OthelloPosition(sys.argv[1])))
